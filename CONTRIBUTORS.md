Epidemio Engine Contributors
===========================


* **[Hidde Schultze](hidde.dev)**
    Please e-mail me with any epidemio related questions @ [epidemio@hidde.me](mailto:epidemio@hidde.me)

* **[Mees Altena](https://www.linkedin.com/in/meesaltena)**
    Contact me at [mees.altena@gmail.com](mailto:mees.altena@gmail.com)
