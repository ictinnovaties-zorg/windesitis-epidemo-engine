﻿using System;
using System.Collections.Generic;
using Epidemo_Engine;
using Epidemo_Engine.BehaviorPackage;
using Epidemo_Engine.DetectorPackage;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Epidemo_Engine.Simulators;
using Xunit;

namespace Epidemo_Engine_Test
{
    public class BehaviorSimulatorTests
    {
        private readonly Simulation _simulation;
        private readonly BehaviorSimulator _behavorSimulator;
        private static readonly string[] Args = new string[5] {"1000", "100", "1", "1", "01903092"};
        private readonly SimulationParameters _params = new SimulationParameters(Args);

        public BehaviorSimulatorTests()
        {
            _simulation = new SimulationMoq(_params) {Parameters = {StartTime = new DateTime(2020, 01, 01)}};
            _behavorSimulator = new BehaviorSimulator(_simulation);
        }

        [Theory]
        [InlineData(0.40, SuspectedState.PossibleCarrier)]
        [InlineData(0.70, SuspectedState.AtRisk)]
        [InlineData(0.80, SuspectedState.ConfirmedCarrier)]
        [InlineData(0, SuspectedState.Safe)]
        public void ShouldSetCorrectContactAvoidanceWhenHasDetector(double percentage, SuspectedState state)
        {
            Person p = new Person(_simulation)
            {
                SuspectedState = state, HasDetector = true, Behavior = {ContactAvoidanceOnSymptoms = 0.5}
            };
            _behavorSimulator.SetContactAvoidance(p);

            Assert.Equal(percentage, p.Behavior.ContactAvoidanceOnSymptoms);
        }

        [Theory]
        [InlineData(0, PersonState.Susceptible)]
        [InlineData(0.6, PersonState.Infected)]
        [InlineData(0, PersonState.Exposed)]
        public void ShouldSetCorrectContactAvoidancePercentageWhenHasNoDetector(double percentage, PersonState state)
        {
            Person p = new Person(_simulation)
            {
                State = state,
                HasDetector = false,
                ContactProperties = {PfOriginal = 0},
                Behavior = {ContactAvoidanceOnSymptoms = 0.5}
            };
            _behavorSimulator.SetContactAvoidance(p);

            Assert.Equal(percentage, p.Behavior.ContactAvoidanceOnSymptoms);
        }

        [Theory]
        [InlineData(0.30, SuspectedState.PossibleCarrier)]
        [InlineData(0.50, SuspectedState.AtRisk)]
        [InlineData(0.80, SuspectedState.ConfirmedCarrier)]
        [InlineData(0, SuspectedState.Safe)]
        public void ShouldSetCorrectSetVaccinationPercentageWhenHasDetector(double percentage, SuspectedState state)
        {
            Person p = new Person(_simulation) {SuspectedState = state, HasDetector = true};
            _behavorSimulator.SetVaccinationPercentage(p);

            Assert.Equal(percentage, p.Behavior.VaccinationOnSymptoms);
        }

        [Theory]
        [InlineData(0.5, PersonState.Susceptible)]
        [InlineData(0.5, PersonState.Infected)]
        [InlineData(0.5, PersonState.Exposed)]
        public void ShouldNotSetVaccinatesOnSymptomsWhenHasNoDetector(double percentage, PersonState state)
        {
            Person p = new Person(_simulation)
            {
                State = state, HasDetector = false, Behavior = {VaccinationOnSymptoms = 0.5}
            };
            _behavorSimulator.SetVaccinationPercentage(p);

            Assert.Equal(percentage, p.Behavior.VaccinationOnSymptoms);
        }

        [Theory]
        [InlineData(0.40, SuspectedState.PossibleCarrier)]
        [InlineData(0.70, SuspectedState.AtRisk)]
        [InlineData(0.80, SuspectedState.ConfirmedCarrier)]
        [InlineData(0, SuspectedState.Safe)]
        public void ShouldSetCorrectSetSeeDoctorOnSymptomsPercentageWhenHasDetector(double percentage,
            SuspectedState state)
        {
            Person p = new Person(_simulation) {SuspectedState = state, HasDetector = true};
            _behavorSimulator.SetSeeDoctorOnSymptomsPercentage(p);

            Assert.Equal(percentage, p.Behavior.SeeDoctorOnSymptoms);
        }

        [Theory]
        [InlineData(0.1, PersonState.Susceptible)]
        [InlineData(0.1, PersonState.Infected)]
        [InlineData(0.1, PersonState.Exposed)]
        public void ShouldNotSetSeeDoctorOnSymptomsPercentageWhenHasNoDetector(double percentage,
            PersonState state)
        {
            Person p = new Person(_simulation) {State = state, HasDetector = false};
            _behavorSimulator.SetSeeDoctorOnSymptomsPercentage(p);

            Assert.Equal(percentage, p.Behavior.SeeDoctorOnSymptoms);
        }

        [Theory]
        [InlineData(SuspectedState.Safe, 0)]
        [InlineData(SuspectedState.AtRisk, 0.7)]
        [InlineData(SuspectedState.ConfirmedCarrier, 0.8)]
        [InlineData(SuspectedState.PossibleCarrier, 0.4)]
        public void ShouldAvoidContactWithDetector(SuspectedState state, double pf)
        {
            _simulation.Parameters.TickMultiplier = 1;
            Person p = new Person(_simulation)
            {
                SuspectedState = state, HasDetector = true, ContactProperties = {Pf = 1}
            };
            _behavorSimulator.SetContactAvoidance(p);
            _behavorSimulator.AvoidContact(p);
            string actual = (p.ContactProperties.Pf).ToString("0.00");
            string expected = pf.ToString("0.00");

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(PersonState.Infected, 0.60)]
        [InlineData(PersonState.Exposed, 1)]
        [InlineData(PersonState.Immune, 1)]
        [InlineData(PersonState.Susceptible, 1)]
        public void ShouldNotAvoidContactWithNoDetector(PersonState state, double pf)
        {
            _simulation.Parameters.TickMultiplier = 1;
            Person p = new Person(_simulation)
            {
                HasDetector = false, ContactProperties = {Pf = 1, PfOriginal = 1}, State = state
            };
            _behavorSimulator.SetContactAvoidance(p);
            _behavorSimulator.AvoidContact(p);

            string actual = (p.ContactProperties.Pf).ToString("0.00");
            string expected = pf.ToString("0.00");

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0, 0, 100, 0, SuspectedState.PossibleCarrier)]
        [InlineData(0, 0, 0, 0, SuspectedState.PossibleCarrier)]
        [InlineData(0, 100, 100, 0, SuspectedState.PossibleCarrier)]
        [InlineData(0, 100, 0, 0, SuspectedState.PossibleCarrier)]
        [InlineData(100, 0, 100, 0, SuspectedState.PossibleCarrier)]
        [InlineData(100, 0, 0, 0, SuspectedState.PossibleCarrier)]
        [InlineData(100, 100, 100, 0, SuspectedState.PossibleCarrier)]
        [InlineData(100, 100, 0, 0, SuspectedState.PossibleCarrier)]
        [InlineData(0, 0, 100, 100, SuspectedState.PossibleCarrier)]
        [InlineData(0, 0, 0, 100, SuspectedState.Safe)]
        [InlineData(0, 100, 100, 100, SuspectedState.ConfirmedCarrier)]
        [InlineData(0, 100, 0, 100, SuspectedState.ConfirmedCarrier)]
        [InlineData(100, 0, 100, 100, SuspectedState.PossibleCarrier)]
        [InlineData(100, 0, 0, 100, SuspectedState.Safe)]
        [InlineData(100, 100, 100, 100, SuspectedState.Safe)]
        [InlineData(100, 100, 0, 100, SuspectedState.Safe)]
        public void ShouldSetCorrectSuspectedStateWhenHasSymptomsWithDetector(double vaccinationOnSymptoms,
            double diagnosisSpecificity, double diagnosisSensitivity, double seeDoctorOnSymptoms, SuspectedState state)
        {
            Person p = new Person(_simulation)
            {
                State = PersonState.Infected,
                HasDetector = true,
                Behavior =
                {
                    DiagnosisSpecificity = diagnosisSpecificity, 
                    SeeDoctorOnSymptoms = seeDoctorOnSymptoms,
                    VaccinationOnSymptoms = vaccinationOnSymptoms, 
                    DiagnosisSensitivity = diagnosisSensitivity
                }
            };

            _behavorSimulator.Behave(p);
            Assert.Equal(state, p.SuspectedState);
        }

        [Theory]
        [InlineData(0, 0, 100, 100, SuspectedState.AtRisk, true)]
        [InlineData(0, 0, 100, 0, SuspectedState.Safe, false)]
        [InlineData(100, 0, 100, 100, SuspectedState.AtRisk, true)]
        [InlineData(100, 0, 100, 0, SuspectedState.Safe, false)]
        [InlineData(0, 100, 100, 100, SuspectedState.ConfirmedCarrier, true)]
        [InlineData(0, 100, 100, 0, SuspectedState.Safe, false)]
        [InlineData(100, 100, 100, 100, SuspectedState.Safe, true)]
        [InlineData(100, 100, 100, 0, SuspectedState.Safe, false)]
        [InlineData(0, 0, 100, 100, SuspectedState.Safe, false)]
        [InlineData(0, 0, 100, 0, SuspectedState.AtRisk, true)]
        [InlineData(100, 0, 100, 100, SuspectedState.Safe, false)]
        [InlineData(100, 0, 100, 0, SuspectedState.AtRisk, true)]
        [InlineData(0, 100, 100, 100, SuspectedState.Safe, false)]
        [InlineData(0, 100, 100, 0, SuspectedState.AtRisk, true)]
        [InlineData(100, 100, 100, 100, SuspectedState.Safe, false)]
        [InlineData(100, 100, 100, 0, SuspectedState.AtRisk, true)]
        [InlineData(0, 0, 0, 100, SuspectedState.Safe, true)]
        [InlineData(0, 0, 0, 0, SuspectedState.Safe, false)]
        [InlineData(100, 0, 0, 100, SuspectedState.Safe, true)]
        [InlineData(100, 0, 0, 0, SuspectedState.Safe, false)]
        [InlineData(0, 100, 0, 100, SuspectedState.ConfirmedCarrier, true)]
        [InlineData(0, 100, 0, 0, SuspectedState.Safe, false)]
        [InlineData(100, 100, 0, 100, SuspectedState.Safe, true)]
        [InlineData(100, 100, 0, 0, SuspectedState.Safe, false)]
        [InlineData(0, 0, 0, 100, SuspectedState.Safe, false)]
        [InlineData(0, 0, 0, 0, SuspectedState.AtRisk, true)]
        [InlineData(100, 0, 0, 100, SuspectedState.Safe, false)]
        [InlineData(100, 0, 0, 0, SuspectedState.AtRisk, true)]
        [InlineData(0, 100, 0, 100, SuspectedState.Safe, false)]
        [InlineData(0, 100, 0, 0, SuspectedState.AtRisk, true)]
        [InlineData(100, 100, 0, 100, SuspectedState.Safe, false)]
        [InlineData(100, 100, 0, 0, SuspectedState.AtRisk, true)]
        public void ShouldSetCorrectSuspectedStateWhenHasNoSymptomsWithDetector(double vaccinationOnSymptoms,
            double diagnosisSpecificity, double diagnosisSensitivity, double seeDoctorOnSymptoms, SuspectedState suspectedState, bool atRisk)
        {
            Person p = new Person(_simulation)
            {
                State = PersonState.Exposed,
                SuspectedState = atRisk ? SuspectedState.AtRisk : SuspectedState.Safe,
                HasDetector = true,
                Behavior =
                {
                    DiagnosisSpecificity = diagnosisSpecificity, 
                    SeeDoctorOnSymptoms = seeDoctorOnSymptoms,
                    VaccinationOnSymptoms = vaccinationOnSymptoms, 
                    DiagnosisSensitivity = diagnosisSensitivity
                }
            };

            _behavorSimulator.Behave(p);
            Assert.Equal(suspectedState, p.SuspectedState);
        }

        [Theory]
        [InlineData(0, 0, 100, PersonState.Infected)]
        [InlineData(0, 0, 0, PersonState.Infected)]
        [InlineData(0, 100, 100, PersonState.Infected)]
        [InlineData(0, 100, 0, PersonState.Infected)]
        [InlineData(100, 0, 100, PersonState.Infected)]
        [InlineData(100, 0, 0, PersonState.Infected)]
        [InlineData(100, 100, 100, PersonState.Immune)]
        [InlineData(100, 100, 0, PersonState.Infected)]
        public void ShouldGetImmuneAfterVaccinationWhenHasNoDetectorButHasSymptoms(double vaccinationOnSymptoms,
            double DiagnosisSpecificity, double seeDoctorOnSymptoms, PersonState state)
        {
            _simulation.Parameters.Disease.InfectionChance = 100;
            Person p = new Person(_simulation)
            {
                State = PersonState.Infected,
                HasDetector = false,
                Behavior =
                {
                    DiagnosisSpecificity = DiagnosisSpecificity,
                    SeeDoctorOnSymptoms = seeDoctorOnSymptoms,
                    VaccinationOnSymptoms = vaccinationOnSymptoms,
                    DiagnosisSensitivity = 0
                }
            };
            _simulation.Parameters.Disease.Compartments.Add(PersonState.Immune);
            _behavorSimulator.Behave(p);

            Assert.Equal(state, p.State);
        }

        [Theory]
        [InlineData(0, 0, 100, PersonState.Exposed)]
        [InlineData(0, 0, 0, PersonState.Exposed)]
        [InlineData(0, 100, 100, PersonState.Exposed)]
        [InlineData(0, 100, 0, PersonState.Exposed)]
        [InlineData(100, 0, 100, PersonState.Exposed)]
        [InlineData(100, 0, 0, PersonState.Exposed)]
        [InlineData(100, 100, 100, PersonState.Exposed)]
        [InlineData(100, 100, 0, PersonState.Exposed)]
        public void ShouldGetImmuneAfterVaccinationWhenHasNoDetectorAndHasNoSymptoms(double vaccinationOnSymptoms,
            double diagnosisSpecificity, double seeDoctorOnSymptoms, PersonState state)
        {
            Person p = new Person(_simulation)
            {
                State = PersonState.Exposed,
                HasDetector = false,
                Behavior =
                {
                    DiagnosisSpecificity = diagnosisSpecificity, SeeDoctorOnSymptoms = seeDoctorOnSymptoms,
                    VaccinationOnSymptoms = vaccinationOnSymptoms, DiagnosisSensitivity = 100
                }
            };

            _behavorSimulator.Behave(p);

            Assert.Equal(state, p.State);
        }

        [Fact]
        public void ShouldNotAffectDeadPeople()
        {
            Person p = new Person(_simulation)
            {
                State = PersonState.Dead,
                HasDetector = true,
            };

            for (int i = 0; i < 10; i++)
            {
                _behavorSimulator.Behave(p);
            }

            Assert.Equal(SuspectedState.Safe, p.SuspectedState);
            Assert.Equal(PersonState.Dead, p.State);
        }

        [Fact]
        public void ShouldReset_PF_Correctly()
        {
            _simulation.Parameters.TickMultiplier = 1;
            Person p = new Person(_simulation)
            {
                State = PersonState.Susceptible,
                SuspectedState = SuspectedState.AtRisk,
                HasDetector = true,
                ContactProperties = new ContactProperties(_simulation) {Pf = 1, PfOriginal = 1},
                Behavior =
                {
                    DiagnosisSpecificity = 0, SeeDoctorOnSymptoms = 100, VaccinationOnSymptoms = 0,
                    DiagnosisSensitivity = 0, ContactAvoidanceOnSymptoms = 100
                },
            };

            Assert.Equal(SuspectedState.AtRisk, p.SuspectedState);
            Assert.Equal(1.ToString("0,0"), (p.ContactProperties.Pf).ToString("0,0"));

            _behavorSimulator.SetContactAvoidance(p);
            _behavorSimulator.Behave(p);

            Assert.Equal(0.7.ToString("0,0"), p.ContactProperties.Pf.ToString("0,0"));

            _simulation.Parameters.Disease.InfectionChance = 0;
            p.Behavior.VaccinationOnSymptoms = 100;
            p.Behavior.DiagnosisSensitivity = 0;
            _behavorSimulator.Behave(p);

            Assert.Equal(SuspectedState.Safe, p.SuspectedState);
            Assert.Equal(1, p.ContactProperties.Pf);
        }
    }
}