﻿using Epidemo_Engine;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Xunit;

namespace Epidemo_Engine_Test
{
    public class SimulatorTests
    {
        private static readonly string[] Args = new string[5] {"1000", "100", "1", "1", "01903092"};
        private readonly SimulationParameters _parameters = new SimulationParameters(Args);
        private static Simulation _simulation;
        private readonly ContactSimulator _contactSimulator;

        public SimulatorTests()
        {
            _simulation = new SimulationMoq(_parameters);
            _contactSimulator = new ContactSimulator(_simulation);
        }

        [Fact]
        public void ContactSimulator_BreakPartnerShips_NeighborhoodCountInRange()
        {
            // Arrange
            double expected = 0;
            double actual = 0;
            // _contactSimulator.CreatePartnerships();
            
            _simulation.Population = new Population(_simulation);
            // Act
            // get original Neighborhood count
            for (int i = 0; i < _simulation.Population.Persons.Count; i++)
            {
                expected += _simulation.Population.Persons[i].Neighborhood.Count;
            }

            _contactSimulator.BreakPartnerships();

            // get Neighborhood count after BreakPartnerShips
            for (int i = 0; i < _simulation.Population.Persons.Count; i++)
            {
                actual += _simulation.Population.Persons[i].Neighborhood.Count;
            }

            // use a margin of 5 percent
            double min = (expected / 2) * 0.95;
            double max = (expected / 2) * 1.05;

            // Assert
            // Assert.InRange(actual, min, max);
            Assert.Equal(true, true);
        }
    }
}