﻿using System;
using System.Collections.Generic;
using Epidemo_Engine;
using Epidemo_Engine.DetectorPackage;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Epidemo_Engine.Simulators;
using Xunit;

namespace Epidemo_Engine_Test
{
    public class DetectorSimulatorTests
    {
        private static readonly string[] Args = new string[5]{"1000","100","1","1","01903092"};
        private readonly SimulationParameters _params = new SimulationParameters(Args);
        private readonly DetectorSimulator _detectorSimulator;
        private readonly Simulation _simulation;

        public DetectorSimulatorTests()
        {
            _simulation = new SimulationMoq(_params) {Parameters = {StartTime = new DateTime(2020, 01, 01)}};
            _detectorSimulator = new DetectorSimulator(_simulation);
        }
        
        // Small scale population test with set population.
        [Theory]
        [InlineData(SuspectedState.PossibleCarrier , SuspectedState.Safe, 1)]
        [InlineData(SuspectedState.ConfirmedCarrier, SuspectedState.AtRisk,2)]
        public void DetectorSimulator_ComputeMessage_MessageIsComputedAndResultsInAction(SuspectedState expectedcarrierState , SuspectedState expectedContactState, int suspectedCount)
        {
            Population userPopulation = new Population(_simulation);
            for (int i = 0; i < 8; i++)
            {
                userPopulation.Persons.Add(new Person(_simulation) { Id = i, State = PersonState.Susceptible, HasDetector = true});
            }
            
            // Making Controlled Connections
            // group 1
            userPopulation.Persons[0].Neighborhood.Add(userPopulation.Persons[1]);
            userPopulation.Persons[1].Neighborhood.Add(userPopulation.Persons[0]);
            userPopulation.Persons[1].Neighborhood.Add(userPopulation.Persons[2]);
            userPopulation.Persons[1].Neighborhood.Add(userPopulation.Persons[3]);
            userPopulation.Persons[0].Neighborhood.Add(userPopulation.Persons[2]);
            userPopulation.Persons[2].Neighborhood.Add(userPopulation.Persons[0]);
            userPopulation.Persons[2].Neighborhood.Add(userPopulation.Persons[1]);
            userPopulation.Persons[2].Neighborhood.Add(userPopulation.Persons[3]);
            userPopulation.Persons[0].Neighborhood.Add(userPopulation.Persons[3]);
            userPopulation.Persons[3].Neighborhood.Add(userPopulation.Persons[0]);
            userPopulation.Persons[3].Neighborhood.Add(userPopulation.Persons[1]);
            userPopulation.Persons[3].Neighborhood.Add(userPopulation.Persons[2]);
            //Group 2.Persons.Persons
            userPopulation.Persons[3].Neighborhood.Add(userPopulation.Persons[4]);
            userPopulation.Persons[3].Neighborhood.Add(userPopulation.Persons[5]);
            userPopulation.Persons[3].Neighborhood.Add(userPopulation.Persons[6]);
            userPopulation.Persons[3].Neighborhood.Add(userPopulation.Persons[7]);
            userPopulation.Persons[4].Neighborhood.Add(userPopulation.Persons[3]);
            userPopulation.Persons[4].Neighborhood.Add(userPopulation.Persons[5]);
            userPopulation.Persons[4].Neighborhood.Add(userPopulation.Persons[6]);
            userPopulation.Persons[4].Neighborhood.Add(userPopulation.Persons[7]);
            userPopulation.Persons[5].Neighborhood.Add(userPopulation.Persons[3]);
            userPopulation.Persons[5].Neighborhood.Add(userPopulation.Persons[4]);
            userPopulation.Persons[5].Neighborhood.Add(userPopulation.Persons[6]);
            userPopulation.Persons[5].Neighborhood.Add(userPopulation.Persons[7]);
            userPopulation.Persons[6].Neighborhood.Add(userPopulation.Persons[3]);
            userPopulation.Persons[6].Neighborhood.Add(userPopulation.Persons[4]);
            userPopulation.Persons[6].Neighborhood.Add(userPopulation.Persons[5]);
            userPopulation.Persons[6].Neighborhood.Add(userPopulation.Persons[7]);
            userPopulation.Persons[7].Neighborhood.Add(userPopulation.Persons[3]);
            userPopulation.Persons[7].Neighborhood.Add(userPopulation.Persons[4]);
            userPopulation.Persons[7].Neighborhood.Add(userPopulation.Persons[5]);
            userPopulation.Persons[7].Neighborhood.Add(userPopulation.Persons[6]);
            
            // Simulate n amount of people being a suspected carrier
            // Note that the actual PersonState is not known to the Detector.
            for (int i = 0; i < suspectedCount; i++)
            {
                userPopulation.Persons[i].SuspectedState = SuspectedState.PossibleCarrier;
            }

            _simulation.Population = userPopulation;
            _detectorSimulator.Execute();
        
            SuspectedState actualCarrierState = userPopulation.Persons[0].SuspectedState;
            SuspectedState actualContactState = userPopulation.Persons[2].SuspectedState;
            
            // Assertions
            Assert.Equal(expectedcarrierState, actualCarrierState);
            Assert.Equal(expectedContactState, actualContactState);
        }
    }
}