﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using Epidemo_Engine;
using Epidemo_Engine.Infector;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Xunit;
using Xunit.Sdk;

namespace Epidemo_Engine_Test
{
    public class InfectorSimulatorTests
    {
        private readonly Simulation _simulation;
        private readonly InfectorSimulator _infectorSimulator;
        private static readonly string[] Args = new string[5] {"1000", "100", "1", "1", "01903092"};
        private readonly SimulationParameters _params = new SimulationParameters(Args);

        public InfectorSimulatorTests()
        {
            _simulation = new SimulationMoq(_params) {Parameters = {StartTime = new DateTime(2020, 01, 01)}};
            _infectorSimulator = new InfectorSimulator(_simulation);
        }
        
        [Theory]
        [InlineData(1, PersonState.Exposed)]
        [InlineData(0, PersonState.Susceptible)]
        public void ShouldSetCorrectPersonStateAfterInfection(double infectionChance, PersonState expectedNeighborState){
            _infectorSimulator.Disease = new Disease(
                1, 
                "testFlu",
                1,
                infectionChance,
                0, new List<PersonState>()
                {
                    PersonState.Susceptible,
                    PersonState.Exposed,
                    PersonState.Infected,
                    PersonState.Recovered,
                });

            Person neighbor = new Person(_simulation) { Id = 2, State = PersonState.Susceptible};

            if (_infectorSimulator.ShouldInfect())
             _infectorSimulator.TryInfect(neighbor);

            PersonState actualNeighborState = neighbor.State;
            Assert.Equal(expectedNeighborState, actualNeighborState);
        }

        [Theory]
        [InlineData(1, 5000)]
        [InlineData(1, 2000)]
        [InlineData(0, 5000)]
        [InlineData(0, 10000)]
        [InlineData(0, 1)]
        public void ShouldSimulateInfectionOverPopulation(double infectionChance, int populationSize){
            _infectorSimulator.Disease = new Disease(
                1, 
                "testFlu",
                1,
                infectionChance,
                0, new List<PersonState>()
                {
                    PersonState.Susceptible,
                    PersonState.Exposed,
                    PersonState.Infected,
                    PersonState.Recovered,
                });

            _simulation.Population = GetNewPopulation(populationSize, populationSize);
            
            _infectorSimulator.Infector();

            double expectedInfectiousPersonCount = populationSize / 100 * 100 * infectionChance + 1;
            int actualInfectiousPersons = _simulation.Population.GetInfectiousPersons().Count();
            Assert.Equal(expectedInfectiousPersonCount, actualInfectiousPersons);
        }

        [Theory]
        [InlineData(0.25, 5000, 5000)]
        [InlineData(0.25, 5000, 2500)]
        [InlineData(0.50, 10000, 5000)]
        [InlineData(0.50, 10000, 1000)]
        [InlineData(0.75, 15000, 5000)]
        [InlineData(0.75, 15000, 3500)]
        [InlineData(1, 150000, 5000)]
        [InlineData(1, 1000, 300)]
        public void ShouldInfectApproximatelyCorrectPercentageOfPopulation(int infectionChange, int populationSize, int neighborhoodSize)
        {
            _infectorSimulator.Disease = new Disease(
                1, 
                "testFlu",
                1,
                infectionChange,
                0, new List<PersonState>()
                {
                    PersonState.Susceptible,
                    PersonState.Exposed,
                    PersonState.Infected,
                    PersonState.Recovered
                }); ;

            double min = (neighborhoodSize / 100 * 100 * infectionChange) - (neighborhoodSize * 0.05);
            double max = (neighborhoodSize / 100 * 100 *  infectionChange) + (neighborhoodSize * 0.05);
            _simulation.Population = GetNewPopulation(populationSize, neighborhoodSize);
            
            int actualExposedPersons = _simulation.Population.GetExposedPersons().Count;
            Assert.Equal(0, actualExposedPersons);
            
            int actualInfectedPersons = _simulation.Population.GetInfectedPersons().Count;
            Assert.Equal(1, actualInfectedPersons);
            
            // Expose neighbors to infection
            _infectorSimulator.Infector();

            actualExposedPersons = _simulation.Population.GetExposedPersons().Count;
            Assert.InRange(actualExposedPersons, min, max);
            
            actualInfectedPersons = _simulation.Population.GetInfectedPersons().Count;
            Assert.Equal(1, actualInfectedPersons);
        }
        
        [Fact]
        public void ShouldSetCorrectInfectionDate()
        {
            _infectorSimulator.Disease = new Disease(
                1, 
                "testFlu",
                1,
                1,
                0, new List<PersonState>()
                {
                    PersonState.Susceptible,
                    PersonState.Infected
                });
            
            _simulation.Population = new Population(_simulation)
            {
                Persons = new List<Person>()
                {
                    new Person(_simulation) { State = PersonState.Infected, Neighborhood = new List<Person>()
                    {
                        new Person(_simulation) { State = PersonState.Susceptible},
                    }},
                }
            };
            
            DateTime infectionDate =  new DateTime(2020, 01, 01);
            _simulation.RealTime = infectionDate;
            _infectorSimulator.Infector();

            Person personBeingInfected = _simulation.Population.Persons[0].Neighborhood[0];
            Assert.Equal(infectionDate, personBeingInfected.InfectionDate);
        }

        private Population GetNewPopulation(int populationSize, int neighborhoodSize)
        {
            Population p = new Population(_simulation);
            Person infectedPerson = new Person(_simulation) {Id = populationSize, State = PersonState.Infected};
            infectedPerson.InfectionDate = new DateTime(1999,01,01);
            p.Persons.Add(infectedPerson);

            for (int i = 0; i < populationSize; i++)
                p.Persons.Add(new Person(_simulation) {Id = i, State = PersonState.Susceptible});
            
            infectedPerson.Neighborhood.AddRange(p.Persons.GetRange(1, neighborhoodSize));
            return p;
        }
     }
}