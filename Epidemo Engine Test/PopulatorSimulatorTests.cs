﻿using System;
using System.Collections.Generic;
using Epidemo_Engine;
using Epidemo_Engine.Infector;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Xunit;

namespace Epidemo_Engine_Test
{
    public class PopulatorSimulatorTests
    {
        private readonly Simulation _simulation;
        private readonly PopulationSimulator _populationSimulator;
        
        public PopulatorSimulatorTests()
        {
            string[] args = new string[5] {"1000", "100", "1", "1", "01903092"};
            SimulationParameters @params = new SimulationParameters(args);
            _simulation = new SimulationMoq(@params)
            {
                Parameters =
                {
                    StartTime = new DateTime(2020, 01, 01), Disease = {FatalityChance = 0}, TickMultiplier = 1
                }
            };
            _populationSimulator = new PopulationSimulator(_simulation);
        }

        [Fact]
        public void ShouldSetCorrectRandomBirthDate()
        {
            Person p = new Person(_simulation);

            DateTime maxDate = _simulation.Parameters.StartTime;
            DateTime min = maxDate.AddYears(-80);

            Assert.InRange(p.BirthDate?.ToString("yyyy MM dd"), min.ToString("yyyy MM dd"),
                maxDate.ToString("yyyy MM dd"));
        }

        [Theory]
        [InlineData(5, PersonState.Exposed)]
        [InlineData(2, PersonState.Infected)]
        public void ShouldSetPersonStateToInfectiousWhenIncubationPeriodOver(int incubationPeriod, PersonState state)
        {
            _simulation.Parameters.Disease = new Disease(
                1,
                "testFlu",
                incubationPeriod,
                0,
                0
                );

            _simulation.Population = new Population(_simulation)
            {
                Persons =
                {
                    new Person (_simulation)
                    {
                        State = PersonState.Exposed,
                        BirthDate = new DateTime(1993, 03, 24),
                        InfectionDate = new DateTime(2020, 01, 01)
                    }
                }
            };
            _simulation.RealTime = new DateTime(2020, 01, 4);
            Assert.Single(_simulation.Population.Persons);

            Person person = _simulation.Population.Persons[0];
            Assert.Equal(PersonState.Exposed, person.State);

            _populationSimulator.TransmitStates();

            Assert.Equal(state, person.State);
        }

        [Theory]
        [InlineData(1950, PersonState.Infected)]
        [InlineData(1850, PersonState.Dead)]
        public void ShouldSetPersonStateToDeadWhenMaxAgeIsReached(int birthYear, PersonState stateAfterModify)
        {
            _simulation.Parameters.Disease.RecoveryChance = 0;
            _simulation.Population = new Population(_simulation)
            {
                Persons =
                {
                    new Person (_simulation)
                    {
                        State = PersonState.Infected,
                        BirthDate = new DateTime(birthYear, 01, 01),
                    }
                }
            };
            _simulation.RealTime = new DateTime(2020, 01, 01);
            Assert.Single(_simulation.Population.Persons);

            _populationSimulator.TransmitStates();

            Person person = _simulation.Population.Persons[0];
            Assert.Equal(stateAfterModify, person.State);
        }

        [Theory]
        [InlineData(PersonState.Immune, true)]
        [InlineData(PersonState.Susceptible, false)]
        public void ShouldSetCorrectRecoveredStateAfterRecoveryPeriodIsOver(PersonState expectedState, bool immuneAfterRecovery)
        {
            _simulation.Parameters.Disease.RecoveryPeriod = 2;
            _simulation.Parameters.Disease.ImmuneAfterRecovery = immuneAfterRecovery;
            _simulation.Parameters.Disease.RecoveryChance = 100;
            _simulation.Parameters.Disease.TickMultiplier = 1;
            _simulation.Parameters.Disease.Compartments.Add(PersonState.Immune);
            _simulation.RealTime = new DateTime(2020, 01, 03);
            
            _simulation.Population = new Population(_simulation)
            {
                Persons =
                {
                    new Person(_simulation) { State = PersonState.Recovered, RecoveryDate = new DateTime(2020, 01, 01) }
                }
            };

            Assert.Equal(2, _simulation.Parameters.Disease.RecoveryPeriod);
            Person person = _simulation.Population.Persons[0];
            
            Assert.Single(_simulation.Population.Persons);
            
            _populationSimulator.TransmitStates();
            
            Assert.Equal(expectedState, person.State);
        }
        
        [Theory]
        [InlineData(0, 0)]
        [InlineData(0.25, 7500)]
        [InlineData(0.50, 75000)]
        [InlineData(0.75, 25000)]
        public void ShouldRecoverApproximatelyCorrectPercentageOfPopulation(double recoveryChange, double infectedPersonCount)
        {
            _simulation.Parameters.Disease = new Disease(
                1,
                "testFlu",
                1,
                0,
                recoveryChange, new List<PersonState>()
                {
                    PersonState.Susceptible, PersonState.Exposed, PersonState.Infected, PersonState.Recovered,
                }) {FatalityChance = 0, RecoveryChance = recoveryChange};

            _simulation.Parameters.Disease.TickMultiplier = 1;
            double min = (infectedPersonCount * (1 - recoveryChange)) - (infectedPersonCount * 0.05);
            double max = (infectedPersonCount * (1 - recoveryChange)) + (infectedPersonCount * 0.05);
            
            _simulation.Population = new Population(_simulation);
            
            for (int i = 0; i < infectedPersonCount; i++)
                _simulation.Population.Persons.Add(new Person(_simulation){Id = i, State = PersonState.Infected});

            int actualInfectiousPersons = _simulation.Population.GetInfectedPersons().Count;
            Assert.Equal(infectedPersonCount, actualInfectiousPersons);
            
            _populationSimulator.TransmitStates();
            
            Assert.InRange(_simulation.Population.GetInfectedPersons().Count, min, max);

            min = ((infectedPersonCount) * recoveryChange) - (infectedPersonCount * 0.05);
            max = ((infectedPersonCount) * recoveryChange) + (infectedPersonCount * 0.05);
            
            int actualRecoveredPersons = _simulation.Population.GetRecoveredPersons().Count;
            Assert.InRange(actualRecoveredPersons, min, max);
        }
    }
}