using System;
using Epidemo_Engine.Exceptions;
using Epidemo_Engine.RunTime;
using Xunit;

namespace Epidemo_Engine_Test
{
    public class RuntimeTests
    {
        [Fact]
        public void SimulationParameters_Constructor_NEqualsArgument(){
            
            string[] args = new string[5]{"1000","100","1","1","01903092"};
            int expected = 1000;
            SimulationParameters parameters = new SimulationParameters(args);

            int actual = parameters.N;
            
            Assert.Equal(expected, actual);
            
        }
        [Fact]
        public void SimulationParameters_Constructor_EpochInputStartTimeEqualsCorrecttime(){
            
            string[] args = new string[6]{"1000","100","1","1","1","1575549724"};
            DateTime expected = new DateTime(2019, 12,5,12,42,04);
            SimulationParameters parameters = new SimulationParameters(args);

            DateTime actual = parameters.StartTime;
            
            Assert.Equal(expected, actual);
            
        }
        [Fact]
        public void SimulationParameters_Constructor_CustomArgLength_Exception()
        {
            //arrange
            string[] args = new string[2]{"1000","100"};
            string expected = "Too little arguments.";
            
            //act
            Action act = () =>
            {
                SimulationParameters parameters = new SimulationParameters(args);
            };
            //assert
            var exception = Assert.Throws<InputArgumentException>(act);
            Assert.Equal(expected, exception.Message);

        }
        [Fact]
        public void SimulationParameters_Constructor_CustomArgFormat_Exception()
        {
            //arrange
            string[] args = new string[4]{"1000","100","dewadew","Text does not belong here"};
            string expected = "Check the format of the input parameters.";
            
            //act
            Action act = () =>
            {
                SimulationParameters parameters = new SimulationParameters(args);
            };
            //assert
            var exception = Assert.Throws<InputArgumentException>(act);
            Assert.Equal(expected, exception.Message);

        }
    }
}