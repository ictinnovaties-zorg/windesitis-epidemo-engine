using System;
using Epidemo_Engine.Exceptions;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Xunit;

namespace Epidemo_Engine_Test
{
    public class SimulationCreationTests
    {
        private static readonly string[] Args = new string[5] {"1000", "100", "1", "10", "01903092"};
        private static readonly SimulationParameters Params = new SimulationParameters(Args);
        private static readonly Simulation S = Simulation.GetInstance(Params);

        private static readonly PopulationProperties PopulationProperties = new PopulationProperties()
        {
            BirthRate = 0.1,
            MaxAge = 100,
            MortalityRate = 0.01,
            N = S.Parameters.N
        };

        PopulationSimulator populationSimulator = new PopulationSimulator(PopulationProperties);

//        [Fact]
//        public void Simulation_Constructor_PopulationGenerated()
//        {
//            int expected = 1000;
//            
//            populationSimulator.GeneratePopulation();
//            int actual = S.Population.Persons.Count;
//
//            Assert.Equal(expected, actual);
//        }
    }
}