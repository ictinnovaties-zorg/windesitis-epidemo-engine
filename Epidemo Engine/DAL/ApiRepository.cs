using System;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Epidemo_Engine.BehaviorPackage;
using Epidemo_Engine.Infector;
using Epidemo_Engine.RunTime;
using Newtonsoft.Json;

namespace Epidemo_Engine.DAL
{
    public static class ApiRepository
    {
        private static readonly HttpClient HttpClient = new HttpClient();

        private static string api_url = "http://localhost:801/api/"; // running the Engine standalone / debugging mode
        // private static string api_url = "http://epidemo_engine_backend_server:801/api/"; // running the Engine in a local docker instance
        // private static string api_url = "http://localhost:5000/api/";
        //private static string api_url = "https://api.epidemio.run/api/";

        static ApiRepository()
        {
            HttpClient.Timeout = TimeSpan.FromMilliseconds(5000);
        }

        public static Disease GetDiseaseById(int id)
        {
            var getDiseaseData = SendGetRequest(typeof(Disease), id);
            if (getDiseaseData != null) return JsonConvert.DeserializeObject<Disease>(getDiseaseData);
            return null;
        }

        public static Behavior GetBehaviourById(int id)
        {
            var getBehaviourData = SendGetRequest(typeof(Behavior), id);
            if (getBehaviourData != null) return JsonConvert.DeserializeObject<Behavior>(getBehaviourData);
            return null;
        }


        /// <summary>
        /// Sends Get Request to endpoint for defined type
        /// </summary>
        /// <param name="t"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException"></exception>
        private static string SendGetRequest(Type t, int id)
        {
            string url = $"{api_url}{t.Name}/{id}";
            try
            {
                using (var response = HttpClient.GetAsync(url).Result)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        Logger.Log("Repository request failed with status code: " + response.StatusCode);
                        throw new HttpRequestException(response.StatusCode.ToString());
                    }

                    // TODO: proper exception handling in case of null response
                    var result = response.Content.ReadAsStringAsync().Result;

                    if (result == null)
                        throw new HttpRequestException(response.StatusCode.ToString());
                    return result;
                }
            }
            catch (Exception e)
            {
                // todo: specific httpclient exception handling
                Console.WriteLine(e);
            }

            return null;
        }


        /// <summary>
        /// Sends post request to type endpoint with payload
        /// </summary>
        /// <param name="t"></param>
        /// <param name="payload"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException"></exception>
        private static dynamic SendPostRequest(Type t, object payload)
        {
            try
            {
                var url = $"{api_url}{t.Name}s";
                using (var response = HttpClient.PostAsync(url,
                    new StringContent(JsonConvert.SerializeObject(payload),
                        Encoding.UTF8,
                        "application/json")).Result)
                {
                    try
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        if (result == null)
                            throw new HttpRequestException(response.StatusCode.ToString());

                        return JsonConvert.DeserializeObject(result);
                    }
                    catch (Exception e)
                    {
                        Logger.Log($"Error while parsing the response from post request to {api_url}");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log($"Error while sending post request to {api_url} :: " + e.Message);
            }

            return null;
        }

        /// <summary>
        /// Sends Put request to type endpoint for identifier with payload
        /// </summary>
        /// <param name="t"></param>
        /// <param name="payload"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// <exception cref="HttpRequestException"></exception>
        private static bool SendPutRequest(Type t, object payload, int Id)
        {
            try
            {
                var url = $"{api_url}{t.Name}s/{Id.ToString()}";
                using (var response = HttpClient.PutAsync(url,
                    new StringContent(JsonConvert.SerializeObject(payload),
                        Encoding.UTF8,
                        "application/json")).Result)
                {
                    try
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        if (result == null || Convert.ToInt32(response.StatusCode) > 300)
                            throw new HttpRequestException(response.StatusCode.ToString());

                        return response.StatusCode == (HttpStatusCode) 204;
                    }
                    catch (Exception e)
                    {
                        Logger.Log($"Error while parsing the response from put request to {api_url}");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log($"Error while sending put request to {api_url}");
            }

            return false;
        }

        /// <summary>
        /// Registers the simulation
        /// </summary>
        /// <param name="simulation"></param>
        /// <returns>Registered SimulationId [int]</returns>
        public static int? RegisterSimulation(Simulation simulation)
        {
            dynamic response =
                SendPostRequest(Simulation.GetInstance().GetType(), ConvertSimulationToObject(simulation));

            if (response != null && response["Id"] != null) return Convert.ToInt32(response["Id"]);
            return null;
        }

        /// <summary>
        /// Converts Simulation entity into base object
        /// </summary>
        /// <param name="simulation"></param>
        /// <returns></returns>
        private static dynamic ConvertSimulationToObject(Simulation simulation)
        {
            return new
            {
                Id = simulation.Id,
                Name = simulation.Parameters.SimulationName,
                N = simulation.Parameters.N,
                T = simulation.Parameters.T,
                SimulationState = simulation.State,
                DiseaseId = (simulation.Parameters.Disease != null) ? simulation.Parameters.Disease.Id : -1,
                simulation.Parameters.DetectorPrevalence,
                BehaviorId = (simulation.Parameters.Behavior != null) ? simulation.Parameters.Behavior.Id : -1,
                simulation.Parameters.TickMultiplier,
                simulation.Parameters.StartTime,
            };
        }


        /// <summary>
        /// Helper function to convert simulation to object and put it's latest version to the server
        /// </summary>
        /// <param name="simulation"></param>
        /// <returns></returns>
        public static bool UpdateSimulation(Simulation simulation = null)
        {
            if (simulation == null) simulation = Simulation.GetInstance();
            bool putStatus =
                SendPutRequest(simulation.GetType(), ConvertSimulationToObject(simulation), simulation.Id);
            return putStatus;
        }
    }
}