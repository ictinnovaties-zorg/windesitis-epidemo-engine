using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Epidemo_Engine.DAL
{
    public class SimulationContext : DbContext
    {
        public DbSet<Population> Populations { get; set; }
//        public DbSet<PersonContact> PersonContact { get; set; }
        public DbSet<Person> Persons { get; set; }
        
        // public DbSet<Metric> Metrics { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //test
            var host = "localhost";
            if (Simulation.GetInstance().Parameters.SimulationName.Contains("Docker")) host = "db";
            Logger.Log($"Configuring DB Connection with {host}");
            string conn_string = $"Server={host};ConnectRetryCount=0;Database=EpidemioDB;User=sa;Password=Research123!";
            optionsBuilder.UseSqlServer(@conn_string);
            optionsBuilder.EnableSensitiveDataLogging();

            // optionsBuilder.UseLoggerFactory(MyLoggerFactory).EnableSensitiveDataLogging();
        }

//        protected override void OnModelCreating(ModelBuilder modelBuilder)
//        {
//            // TODO: Add delete behaviour
//
//            modelBuilder.Entity<PersonContact>()
//                .HasOne(p => p.Person)
//                .WithMany(p => p.Contacts)
//                .HasForeignKey(p => p.PersonId)
//                .OnDelete(DeleteBehavior.NoAction);
//        }
    }

}