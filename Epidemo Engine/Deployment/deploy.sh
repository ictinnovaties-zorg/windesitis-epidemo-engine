#!/usr/bin/env bash

export BITBUCKET_BRANCH=$1
export EPDIO_ENV=$2
export EPDIO_N=$3
export EPDIO_T=$4
export EPDIO_DID=$5
export EPDIO_BID=$6
export EPDIO_Tick=$7
export EPDIO_Start=$8
export EPDIO_Name=$1

echo "Deploying branch $BITBUCKET_BRANCH via user $USER on env $EPDIO_ENV with variables N=$EPDIO_N T=$EPDIO_T D=$EPDIO_DID B=$EPDIO_DID Tick=$EPDIO_Tick Start=$EPDIO_Start"

mkdir -p /home/epidemio/$BITBUCKET_BRANCH/
cd /home/epidemio/$BITBUCKET_BRANCH/

if cd windesitis-epidemo-engine; then git checkout $BITBUCKET_BRANCH && git pull; else git clone --single-branch --branch $BITBUCKET_BRANCH git@bitbucket.org:ictinnovaties-zorg/windesitis-epidemo-engine.git && cd windesitis-epidemo-engine; fi
cd "Epidemo Engine"

docker build -t windesheim_epidemo_engine_image -f Dockerfile .

docker-compose down
if [[ "$EPDIO_ENV" = "production" ]]; then
    docker-compose up -d --scale epidemo_engine=10
elif [[ "$BITBUCKET_BRANCH" = "develop" ]]; then
    docker-compose up -d --scale epidemo_engine=10
else
    docker-compose up -d
fi