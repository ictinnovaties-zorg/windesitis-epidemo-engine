using System;

namespace Epidemo_Engine.Exceptions
{
    public class InputArgumentException : Exception
    {
        public InputArgumentException(string message) : base(message)
        {
        }

        public override string ToString()
        {
            return Message +
                   "\n\nThe Engine excepts a minimal of 3 parameters as input:\n[N] [T] [Disease ID] [Behavior ID]\nWith optional [TickRate (default: 1) (0.1 .. *)] [StartTime (default: now) (epoch) [DetectorPrevalence (default: 1)]";
        }
    }
}