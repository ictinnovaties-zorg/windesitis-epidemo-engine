using System;

namespace Epidemo_Engine.Exceptions
{
    public class SimulationNotCreatedException : Exception
    {
        public SimulationNotCreatedException()
        {
        }

        public override string ToString()
        {
            return Message +
                   "\n\nNo instance of simulation has been instantiated.";
        }
    }
}