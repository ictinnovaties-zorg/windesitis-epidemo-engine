using System;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine
{
    public static class Logger
    {
        
        private static Simulation _simulation = Simulation.GetInstance();
        /// <summary>
        /// Log string to console, with type and simulator option
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type">LogItemType can either be General,Error,Exception or Critical</param>
        /// <param name="sim">Simulator instance, that 'll be used to get the name where the message is coming from</param>
        public static void Log(string message, LogItemType type = LogItemType.General, ISimulator sim = null)
        {
            DateTime dt;
            try
            { 
                dt = _simulation.RealTime;
            }
            catch (Exception e)
            {
                dt = new DateTime();
            }
            Console.WriteLine(
                $"{dt.ToString()}.{dt.Millisecond.ToString()} :: [{(sim != null ? $"{type.ToString()} @ {sim.GetType().Name}" : type.ToString())}] :: {message}");
        }

        public static void Log(object obj, LogItemType type = LogItemType.General, ISimulator sim = null)
        {
            Log(obj.ToString(), type, sim);
        }

        /// <summary>
        /// Overloads the default 'l' function with the Exception Message readout 
        /// </summary>
        /// <param name="e"></param>
        /// <param name="sim"></param>
        public static void Log(Exception e, ISimulator sim = null)
        {
            Log(e, LogItemType.Exception, sim);
        }
    }

    public enum LogItemType
    {
        General,
        Error,
        Exception,
        Critical,
    }
}