﻿namespace Epidemo_Engine.BehaviorPackage
{
    public class Behavior
    {
        // ID of behavior
        public int Id { get; set; }

        // Factor by which a persons Pf (percentage that a new contact is made) is multiplied when a who notices symptoms, as they avoid contact with any others until recovered.
        public double ContactAvoidanceOnSymptoms { get; set; }

        // Vaccination on symptoms: chance [%] that a person who notices symptoms, gets vaccinated
        public double VaccinationOnSymptoms { get; set; }
        
        // Diagnosis on symptoms: chance [%] that a person who notices symptoms, goes to a doctor to get diagnosis
        public double SeeDoctorOnSymptoms { get; set; }

        // Diagnosis specificity: true positive rate [%] for a doctor diagnosis
        public double DiagnosisSpecificity { get; set; }

        // Diagnosis sensitivity: true negative rate [%] for a doctor diagnosis
        public double DiagnosisSensitivity { get; set; }


        public Behavior(int id, double contactAvoidanceOnSymptoms, double vaccinationOnSymptoms, double seeDoctorOnSymptoms,
            double diagnosisSpecificity, double diagnosisSensitivity)
        {
            Id = id;
            ContactAvoidanceOnSymptoms = contactAvoidanceOnSymptoms;
            VaccinationOnSymptoms = vaccinationOnSymptoms;
            SeeDoctorOnSymptoms = seeDoctorOnSymptoms;
            DiagnosisSpecificity = diagnosisSpecificity;
            DiagnosisSensitivity = diagnosisSensitivity;
          
        }

        public Behavior() {}

        public override string ToString()
        {
            return
                $"        ID: {Id}\n" +
                $"        ContactAvoidanceOnSymptoms: {ContactAvoidanceOnSymptoms}\n" +
                $"        VaccinationOnSymptoms: {VaccinationOnSymptoms}\n" +
                $"        DiagnosisOnSymptoms {SeeDoctorOnSymptoms}\n " +
                $"        DiagnosisSpecificity: {DiagnosisSpecificity}\n" +
                $"        DiagnosisSensitivity: {DiagnosisSensitivity}\n";
        }
    }
}