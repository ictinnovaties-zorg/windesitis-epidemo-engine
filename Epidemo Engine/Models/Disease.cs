﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine.Infector
{
    public class Disease
    {

        [NotMapped] public double TickMultiplier { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<PersonState> Compartments { get; set; } //Type of states disease can apply to an individual.
        public int IncubationPeriod { get; set; }
        [NotMapped] private double _fatalityChance { get; set; } // dividing the number of deaths from a specified disease over a defined period of time by the number of individuals diagnosed with the disease during that time; 
        public double FatalityChance
        {
            get => _fatalityChance * TickMultiplier;
            set => _fatalityChance = value;
        }

        [NotMapped] private double _infectionChance;
        public double InfectionChance
        {
            get => _infectionChance * TickMultiplier;
            set => _infectionChance = value;
        }

        [NotMapped] private double _recoveryChance;
        public double RecoveryChance
        {
            get => _recoveryChance * TickMultiplier;
            set => _recoveryChance = value;
        }
        
//        public double RecoveryChance { get; set; }
//        public int RecoveryPeriod { get; set; }

        [NotMapped] private double _recoveryPeriod;
        public double RecoveryPeriod
        {
            get => _recoveryPeriod * TickMultiplier;
            set => _recoveryPeriod = value;
        }
        public bool ImmuneAfterRecovery { get; set; }

        public Disease(
            int id,
            string name,
            int incubationPeriod,
            double infectionChance,
            double recoveryChance,
            List<PersonState> compartments = null,
            int recoveryPeriod = 7,
            bool immuneAfterRecovery = false,
            double fatalityChance = 0)
        {
            TickMultiplier = 1;
            Id = id;
            Name = name;
            Compartments = compartments;
            IncubationPeriod = incubationPeriod;
            _fatalityChance = fatalityChance;
            InfectionChance = infectionChance;
            RecoveryChance = recoveryChance;
            ImmuneAfterRecovery = immuneAfterRecovery;
            RecoveryPeriod = recoveryPeriod;
        }

        public Disease()
        {
        }

        public override string ToString()
        {
            return
                "       iD: " + Id.ToString() + "\n" +
                "       name: " + Name.ToString() + "\n" +
                "       compartments: " + String.Join(",",Compartments.ToList()) + "\n" +
                "       incubationPeriod: " + IncubationPeriod.ToString() + "\n" +
                "       fatalityRate: " + FatalityChance.ToString() + "\n" +
                "       infectionChance: " + InfectionChance.ToString() + "\n" +
                "       recoveryChance: " + RecoveryChance.ToString() + "\n";
        }
    }
}