using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine.DAL
{
    public class Metric
    {
        
        /// <summary>
        /// The of the simulation.
        /// </summary>
        public int SimulationId { get; set; }
        public int PopulationId { get; set; }

        /// <summary>
        /// The actual, real-world time this snapshot was taken
        /// </summary>
        ///
        public DateTime TimeOfSnapshot { get; set; }

        /// <summary>
        /// The simulation RealTime time 
        /// </summary>
        public DateTime RealTime { get; set; }
        
        /// <summary>
        /// The TickMultiplier of the simulation
        /// </summary>
        public double TickMultiplier { get; set; }

        public DateTime StartTime { get; set; }

        /// <summary>
        /// The size of the population
        /// </summary>
        public int PopulationSize { get; set; }


        public int TotalInfected { get; set; }
        public int TotalInfectious { get; set; }
        public int TotalRecovered { get; set; }
        public int TotalExposed { get; set; }
        public int TotalSusceptible { get; set; }
        public int TotalImmune { get; set; }
        public int TotalDead { get; set; }
        // End state compartment metrics
        
        // Start suspected state compartment metrics
        public int TotalSuspectedSafe { get; set; }
        public int TotalSuspectedAtRisk { get; set; }
        public int TotalSuspectedPossibleCarriers { get; set; }
        public int TotalSuspectedConfirmedCarriers { get; set; }
        // End state compartment metrics
        public double DetectorPrevalence { get; set; }
        
        public int PersonsWithDetector { get; set; }
        public double AveragePf { get; set; }
        public double AveragePb { get; set; }
        public double AveragePartnerships { get; set; }
        public double MaxPartnerships { get; set; }
        public string DiseaseName { get; set; }
        public int BehaviourId { get; set; }

        /// <summary>
        /// The duration of the last tick
        /// </summary>
        public double LastTickDuration { get; set; }
        public int Tick { get; set; }

        [NotMapped] private Simulation Simulation;

        public Metric(int tick, double lastTickDuration)
        {
            Simulation = Simulation.GetInstance();
            PopulationId = Simulation.Population.Id;
            SimulationId = Simulation.Id;
            TimeOfSnapshot = DateTime.Now;
            RealTime = Simulation.RealTime;
            TickMultiplier = Simulation.Parameters.TickMultiplier;
            StartTime = Simulation.Parameters.StartTime;
            PopulationSize = Simulation.Population.Persons.Count;
            
            TotalInfected = Simulation.Population.GetInfectedPersons().Count;
            TotalInfectious = Simulation.Population.GetInfectiousPersons().Count;
            TotalRecovered = Simulation.Population.GetRecoveredPersons().Count;
            TotalExposed = Simulation.Population.GetExposedPersons().Count;
            TotalSusceptible = Simulation.Population.GetSusceptiblePersons().Count;
            TotalImmune = Simulation.Population.GetImmunePersons().Count;
            TotalDead = Simulation.Population.GetDeadPersons().Count;
            
            TotalSuspectedSafe = Simulation.Population.GetSuspectedSafePersons().Count;
            TotalSuspectedAtRisk = Simulation.Population.GetSuspectedAtRiskPersons().Count;
            TotalSuspectedPossibleCarriers = Simulation.Population.GetSuspectedPossibleCarrierPersons().Count;
            TotalSuspectedConfirmedCarriers = Simulation.Population.GetSuspectedConfirmedCarrierPersons().Count;

            DetectorPrevalence = Simulation.Parameters.DetectorPrevalence;
            PersonsWithDetector = Simulation.Population.GetPersonsWithDetector().Count;
            
            AveragePf = Simulation.Population.GetAveragePf();
            AveragePb = Simulation.Population.GetAveragePb();
            AveragePartnerships = Simulation.Population.AvgPartnerships();
            MaxPartnerships = Simulation.Population.GetMaxPartnerships();
            DiseaseName = Simulation.Parameters.Disease.Name;
            BehaviourId = Simulation.Parameters.Behavior.Id;
            LastTickDuration = lastTickDuration;
            Tick = tick;
        }
        
    }
}