using System;
using System.Collections.Generic;
using Epidemo_Engine.BehaviorPackage;
using Epidemo_Engine.DetectorPackage;
using Epidemo_Engine.Simulators;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Epidemo_Engine.DAL;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine.Populator
{
    public class Person
    {
        [Key] public int Id { get; set; }
        [NotMapped] public List<Person> Neighborhood { get; set; }
        [NotMapped] public List<PersonPartnership> Partnerships { get; set; }

        // TODO: Figure out which properties need to be mapped
        public PersonState State { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? InfectionDate { get; set; }
        public DateTime? RecoveryDate { get; set; }
        public SuspectedState SuspectedState { get; set; }

        [NotMapped] public Behavior Behavior;

        [NotMapped] public ContactProperties ContactProperties;

        [NotMapped] public bool HasDetector;
        [NotMapped] private SuspectedState _suspectedState;

        [NotMapped] public Simulation simulation { get; set; }

        public Person(int id, PersonState state, bool detector): this(id, state)
        {
            HasDetector = detector;
        }
        
        public Person(int id, PersonState state): this()
        {
            Id = id;
            State = state;
        }

        public Person() : this(Simulation.GetInstance()) {}

        public Person(Simulation sim)
        {
            simulation = sim;
            Neighborhood = new List<Person>();
            BirthDate = GetRandomBirthDate();
            Behavior = sim.Parameters.Behavior;
            HasDetector = sim.Parameters.Random.Next(0,100) <= (sim.Parameters.DetectorPrevalence*100);
            SuspectedState = SuspectedState.Safe;
            ContactProperties = new ContactProperties(sim)
            {
                Mp = 5,
                Pb = 0.2,
                Pf = 0.2,
                Pr = 0.2,
                PfOriginal = 0.2
            };
        }

        private DateTime GetRandomBirthDate()
        {
            DateTime maxDate = simulation.Parameters.StartTime;
            long maxTicks = maxDate.Ticks;
            long minTicks = maxDate.AddYears(-80).Ticks;
            long baseTicks = maxTicks-minTicks;

            long toAdd = (long)(simulation.Parameters.Random.NextDouble()*baseTicks);

            DateTime newDate = new DateTime(minTicks+toAdd);
            return newDate;
        }
    }
}