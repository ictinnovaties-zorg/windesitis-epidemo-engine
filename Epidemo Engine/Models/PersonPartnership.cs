using System.ComponentModel.DataAnnotations;
using System.Linq;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine.Populator
{
    public class PersonPartnership
    {
        public PersonPartnership()
        {
        }

        public PersonPartnership(Person p1, Person p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        [Key] public int Id { get; set; }


        public int Person1Id
        {
            get { return p1.Id; }
            set { p1 = Simulation.GetInstance().Population.Persons.Where(p => p.Id == value).FirstOrDefault(); }
        }

        public int Person2Id
        {
            get { return p2.Id; }
            set { p2 = Simulation.GetInstance().Population.Persons.Where(p => p.Id == value).FirstOrDefault(); }
        }

        public Person p1;
        public Person p2;
    }
}