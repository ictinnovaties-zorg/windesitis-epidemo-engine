namespace Epidemo_Engine.Populator
{
//    public enum PersonState
//    {
//        // All possible states for a person to be in
//        // Covers most diseases
//        // It's unlikely all of these will be used
//        Susceptible = 0, // Healthy, susceptible to infection
//        Infectious = 1, // Infected, can infect others
//        Recovered = 2, // Healthy again
//        Exposed = 3, // Incubation period, person is infected and asymptomatic but not yet infectious 
//        ImmunePassive = 4, // Immunity caused by antibodies, e.g. maternal immunity after birth
//        ImmuneActive = 5, // Immunity caused by injection of antigens, e.g. via vaccination
//        Carrier = 6 // A state of being infectious, but asymptomatic.
//    }
    
    public enum PersonState
    {
        // Covers most diseases
        Susceptible = 0, // Healthy, susceptible to infection
        Exposed = 1, // Incubation period, person is infected, asymptomatic and infectious 
        Infected = 2, // Infected, has symptoms, can infect others
        Recovered = 3, // Healthy again, not susceptible period
        Immune = 4, // Immune by antibodies or vaccine.
        Dead = 5,
    }
}