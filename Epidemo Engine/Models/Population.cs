using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Epidemo_Engine.DetectorPackage;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine.Populator
{
    public class Population
    {
        [Key] public int Id { get; set; }
        public List<Person> Persons { get; set; }
        [NotMapped] private readonly Simulation _simulation;
        [NotMapped] public List<Person> UpdatedPersons { get;}
        [NotMapped]  public List<PersonPartnership> UpdatedPartnerships { get; }


        public Population(Simulation simulation)
        {
            _simulation = simulation;
            Persons = new List<Person>();
            UpdatedPersons = new List<Person>();
            UpdatedPartnerships = new List<PersonPartnership>();
        }

        public Population() : this(Simulation.GetInstance()) {}

        public void Stop()
        {
            _simulation.Stop();
        }

        // Calculates the local clustering coefficient for a given person.
        public double LocalClusteringCoefficient(Person v)
        {
            var ni = v.Neighborhood;
            int ki = ni.Count;
            double max = (double) (ki * (ki - 1)) / 2;

            HashSet<ValueTuple<int, int>> links = new HashSet<(int, int)>();

            for (var i = 0; i < ni.Count; i++)
            {
                for (var i1 = 0; i1 < ni.Count; i1++)
                {
                    if (ni[i].Neighborhood.Contains(ni[i1]))
                    {
                        links.Add((ni[i].Id, ni[i1].Id));
                        links.Add((ni[i1].Id, ni[i].Id));
                    }
                }
            }

            double actual = (double) links.Count / 2;
            return actual / max;
        }

        // Estimates the global clustering coefficient. Gets more accurate with a higher amount of trials. (default 1000)
        // Works by getting a random person, getting two of its neighbors and seeing if they're connected. 
        // (This is likely to be 0 with a trial size of about < 100000)
        public double GlobalClusteringCoefficient(double trials)
        {
//            if (trials > Persons.Count)

            trials = Persons.Count;
            double triangles = 0;
            for (int i = 0; i < trials; i++)
            {
//                if (Persons[i].Neighborhood.Count < 2)
//                    continue;

                var p1 = Persons[_simulation.Parameters.Random.Next(Persons.Count)];
                var p2 = Persons[_simulation.Parameters.Random.Next(Persons.Count)];

                if (p1.Neighborhood.Contains(p2))
                    triangles++;
            }

            return triangles / trials;
//            return triangles;
        }

        public double AvgPartnerships()
        {
            return Persons.Select(p => p.Neighborhood.Count).Average();
        }

        // Prints the average number of partnerships in the population, and the max amount of partnerships.
        public void GetAverageNumberOfPartnerships()
        {
            double avg = Persons.Select(p => p.Neighborhood.Count).Average();
            double max = Persons.Max(p => p.Neighborhood.Count);

            Console.WriteLine($"Average number of partnerships: {avg}. Max number of partnerships: {max}");
        }

        // Prints the total individuals in each state that exists in the population, and the occurrence percentage.
        public void CompartmentRatios()
        {
            Dictionary<PersonState, double> states = new Dictionary<PersonState, double>();
            Persons.ForEach(p =>
            {
                if (!states.ContainsKey(p.State))
                    states.Add(p.State, 1);
                else
                    states[p.State]++;
            });

            foreach (var keyValuePair in states)
                Console.WriteLine(
                    $"{keyValuePair.Key} appeared {keyValuePair.Value} times. {(keyValuePair.Value / Persons.Count) * 100}%");
        }

        // Start state compartments
        public List<Person> GetInfectiousPersons() => Persons
            .Where(person => person.State == PersonState.Infected || person.State == PersonState.Exposed).ToList();
        public List<Person> GetInfectedPersons() =>
            Persons.Where(person => person.State == PersonState.Infected).ToList();
        public List<Person> GetExposedPersons() =>
            Persons.Where(person => person.State == PersonState.Exposed).ToList();
        public List<Person> GetRecoveredPersons() =>
            Persons.Where(person => person.State == PersonState.Recovered).ToList();
        public List<Person> GetSusceptiblePersons() =>
            Persons.Where(person => person.State == PersonState.Susceptible).ToList();
        public List<Person> GetImmunePersons() =>
            Persons.Where(person => person.State == PersonState.Immune).ToList();
        public List<Person> GetDeadPersons() =>
            Persons.Where(person => person.State == PersonState.Dead).ToList();
        // End state compartments
        
        // Start suspected state compartments
        public List<Person> GetSuspectedSafePersons() =>
            Persons.Where(person => person.SuspectedState == SuspectedState.Safe).ToList();
        public List<Person> GetSuspectedAtRiskPersons() =>
            Persons.Where(person => person.SuspectedState == SuspectedState.AtRisk).ToList();
        public List<Person> GetSuspectedPossibleCarrierPersons() =>
            Persons.Where(person => person.SuspectedState == SuspectedState.PossibleCarrier).ToList();
        public List<Person> GetSuspectedConfirmedCarrierPersons() =>
            Persons.Where(person => person.SuspectedState == SuspectedState.ConfirmedCarrier).ToList();
        // End suspected state comartments

        public double GetAveragePf() => Persons.Average(p => p.ContactProperties.Pf);
        public double GetAveragePb() => Persons.Average(p => p.ContactProperties.Pb);
        public List<Person> GetPersonsWithDetector() =>
            Persons.Where(person => person.HasDetector == true).ToList();

        public double GetMaxPartnerships() => Persons.Max(p => p.Neighborhood.Count);
        public override string ToString()
        {
            return "Population Size: " + Persons.Count.ToString();
        }

    }
}