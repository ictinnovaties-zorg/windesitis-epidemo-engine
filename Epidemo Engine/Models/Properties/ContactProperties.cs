using System.Collections.Generic;
using System.Linq;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine
{
    public class ContactProperties
    {
        public ContactProperties() : this(Simulation.GetInstance()) {}

        public ContactProperties(Simulation sim)
        {
            TickMultiplier = sim.Parameters.TickMultiplier;
        }

        // All properties are for ticks of 1 hour, if tickmultiplier is 2, ticks are 2 hours for example
        public double TickMultiplier { get; set; }

        /// <summary>
        /// Maximum number of partners per individual.
        /// </summary>
        public double Mp;

        /// <summary>
        /// Probability that a new partner is selected using the Random Mixing (RM) method (0.5 = 50%).
        /// </summary>
        public double Pr { get; set; }

        private double _pb;

        /// <summary>
        /// Probability that a partnership is broken per time step (0.5 = 50%).
        /// </summary>
        public double Pb
        {
            get => _pb * TickMultiplier;
            set => _pb = value;
        }
        
        /// <summary>
        /// The original Pf value, temp
        /// </summary>
        public double PfOriginal { get; set; }

        private double _pf;

        /// <summary>
        /// Probability that a new partnership is formed per time step (0.5 = 50%).
        /// </summary>
        public double Pf
        {
            get => _pf * TickMultiplier;
            set => _pf = value;
        }
    }
}