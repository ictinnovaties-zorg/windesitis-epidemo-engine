using Epidemo_Engine.RunTime;

namespace Epidemo_Engine.Populator
{
    public class PopulationProperties
    {
        public PopulationProperties()
        {
            TickMultiplier = Simulation.GetInstance().Parameters.TickMultiplier;
        }

        private double TickMultiplier { get; set; }

        /// <summary>
        /// Size of the population.
        /// </summary>
        public int N { get; set; }

        /// <summary>
        /// Base death rate per time step (0.5 = 50%).
        /// </summary>
        private double _mortalityRate;
        public double MortalityRate
        {
            get => _mortalityRate * TickMultiplier;
            set => _mortalityRate = value;
        }

        /// <summary>
        /// Maximum age for an individual in ticks (10 = 10 hours).
        /// </summary>
        private double _maxAge;
        public double MaxAge
        {
            get => _maxAge * TickMultiplier;
            set => _maxAge = value;
        }

        /// <summary>
        /// Base birth rate per time step.
        /// </summary>
        private double _birthRate;
        public double BirthRate
        {
            get => _birthRate * TickMultiplier;
            set => _birthRate = value;
        }
        
        /// <summary>
        /// Percentage of the population that has a detector, value between 0 and 100
        /// </summary>
        public double DetectorPrevalence { get; set; }
    }
}