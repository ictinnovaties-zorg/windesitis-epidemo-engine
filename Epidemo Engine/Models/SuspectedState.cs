﻿namespace Epidemo_Engine.DetectorPackage
{
    public enum SuspectedState
    {
        // All possible states the Detector can suspect a Person of being.
        // Determines if the Person in question is at risk, possibly infected or definitely carrying the disease.
        Safe = 0, // Person is at no known risk of being infected.
        AtRisk = 1, // Person runs the risk of being infected by a (possible) carrier.
        PossibleCarrier = 2, // Person is suspected to be carrying a disease.
        ConfirmedCarrier = 3, // Person is a confirmed carrier of a disease.
    }
}