﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Epidemo_Engine.DAL;
using Epidemo_Engine.Exceptions;
using Epidemo_Engine.RunTime;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SocketIOClient;

namespace Epidemo_Engine
{
    class Program
    {
        static void Main(string[] args)
        {
            //     ______ ____   ____ ____   ______ __  ___ III____     ______ _   __ ______ ____ _   __ ______
            //    / ____// __ \ /  _// __ \ / ____//  |/  /III/ __ \   / ____// | / // ____//  _// | / // ____/
            //   / __/  / /_/ / / / / / / // __/  / /|_/ IIII// / / /  / __/  /  |/ // / __  / / /  |/ // __/   
            //  / /___ / ____/_/ / / /_/ // /___ / /  / /IIII/ /_/ /  / /___ / /|  // /_/ /_/ / / /|  // /___   
            // /_____//_/    /___//_____//_____//_/  /_/ IIII\____/  /_____//_/ |_/ \____//___//_/ |_//_____/
            //
            // 🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀
            // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥

            // if (debuggingMode)
            // {
            Console.WriteLine("Raw Input parameters: " + String.Join(",", args) + "\n\n");
            // }

            AppDomain.CurrentDomain.ProcessExit += (s,e) => { Console.WriteLine("Epidemio Engine Exiting..."); };
            
            AsyncMain(args);

            Thread.Sleep(Timeout.Infinite);
        }

        private static bool InitDb()
        {
            try
            {
                using (SimulationContext db = new SimulationContext())
                {
                    if (db.Database.CanConnect())
                    {
                        db.Database.EnsureCreated();
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                // Console.WriteLine(e);
            }

            return false;
        }


        private static async Task AsyncMain(string[] args)
        {
            try
            {
                SimulationParameters parameters = new SimulationParameters(args);
                Simulation simulation1 = Simulation.GetInstance(parameters);

                // while (!InitDb())
                // {
                //     Logger.Log("Database connection not available, retrying in 1 sec.");
                //     await Task.Delay(1000);
                // }

                Logger.Log("Created Simulation Instance: ");
                Logger.Log(simulation1);

                await simulation1.RegisterSimulation();
                if (simulation1.State == SimulationState.Waiting)
                {
                    simulation1.Initialize();
                    simulation1.InitializePopulation();
                    await simulation1.SimulationTask();
                }
                else
                {
                    Logger.Log("Waiting for initialization...");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}