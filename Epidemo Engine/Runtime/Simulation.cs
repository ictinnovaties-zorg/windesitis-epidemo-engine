using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using EFCore.BulkExtensions;
using Epidemo_Engine.DAL;
using Epidemo_Engine.Exceptions;
using Epidemo_Engine.Populator;
using Epidemo_Engine.Simulators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using SocketIOClient;
using Timer = System.Timers.Timer;

namespace Epidemo_Engine.RunTime
{
    public class Simulation
    {
        private static Simulation _instance;

        // Id of the simulation, proviced by the backend when registering the simulation in RegisterSimulation 
        public int Id { get; internal set; }

        public SimulationState State
        {
            get => _state;
            set
            {
                _state = value;
                Socket.SimulationEvent("state_update", new
                {
                    Id = Id,
                    State = value
                });
                
                if (_instance != null)
                {
                    ApiRepository.UpdateSimulation(_instance);
                }
            }
        }

        public SimulationParameters Parameters { get; set; }
        public int Ticks { get; internal set; }
        public DateTime RealTime;
        public List<ISimulator> SimulatorRunners { get; set; }


        private DateTime LastTick = DateTime.Now;
        private Population _population;
        private int TimerSpeed { get; set; } = 500;

        public Population Population { get; set; }

        private static readonly HttpClient HttpClient = new HttpClient();
        private readonly SimulationSocket Socket = new SimulationSocket();


        public static Simulation GetInstance(SimulationParameters parameters)
        {
            if (_instance == null)
            {
                _instance = new Simulation(parameters);
            }

            return _instance;
        }

        public static Simulation GetInstance()
        {
            if (_instance == null)
            {
                throw new SimulationNotCreatedException();
            }

            return _instance;
        }

        public void Initialize()
        {
            InfectorSimulator infectorSimulator = new InfectorSimulator();
            BehaviorSimulator behaviorSimulator = new BehaviorSimulator();
            DetectorSimulator detectorSimulator = new DetectorSimulator();

            RealTime = Parameters.RealTime;

            // Generate population, contact, infector simulators
            PopulationSimulator populationSimulator = new PopulationSimulator(new PopulationProperties()
            {
                BirthRate = 0.1,
                MaxAge = 100,
                MortalityRate = 0.01,
                N = Parameters.N,
                DetectorPrevalence = Parameters.DetectorPrevalence
            });

            ContactSimulator contactSimulator = new ContactSimulator();

            SimulatorRunners = new List<ISimulator>();
            SimulatorRunners.Add(populationSimulator);
            SimulatorRunners.Add(contactSimulator);
            SimulatorRunners.Add(detectorSimulator);
            SimulatorRunners.Add(behaviorSimulator);
            SimulatorRunners.Add(infectorSimulator);
        }


        public async Task RegisterSimulation()
        {
            int? nId = ApiRepository.RegisterSimulation(this);
            if (nId != null)
            {
                Id = (int) nId;
                Logger.Log("Simulation registered, Id: " + Id);
                await Socket.Connect();
            }
        }

        private SocketIO public_socket;
        private SocketIO socket_simulation;
        private SimulationState _state = SimulationState.WaitingForInitialization;


        public void InitializePopulation()
        {
            Population.Persons.Clear();
            ((PopulationSimulator) SimulatorRunners[0]).GeneratePopulation();
            Population.Persons[Parameters.Random.Next(Parameters.N)].State = PersonState.Infected;
            Population.Persons[Parameters.Random.Next(Parameters.N)].State = PersonState.Infected;
            Population.Persons[Parameters.Random.Next(Parameters.N)].State = PersonState.Infected;
            Population.Persons[Parameters.Random.Next(Parameters.N)].State = PersonState.Exposed;
            Population.Persons[Parameters.Random.Next(Parameters.N)].State = PersonState.Exposed;
            Population.Persons[Parameters.Random.Next(Parameters.N)].State = PersonState.Exposed;
            Population.Persons[Parameters.Random.Next(Parameters.N)].State = PersonState.Infected;
            Population.Persons[Parameters.Random.Next(Parameters.N)].State = PersonState.Infected;

            // try
            // {
            //     using (var db = new SimulationContext())
            //     {
            //         // Empty metrics, temporary
            //         // TODO: remove this truncate
            //         db.Database.ExecuteSqlCommand("TRUNCATE TABLE Metrics");
            //         db.BulkInsert(Population.Persons, new BulkConfig()
            //         {
            //             UseTempDB = false,
            //             PropertiesToInclude = new List<string>() {"Id", "State", "SuspectedState"}
            //         });
            //     }
            // }
            // catch (Exception e)
            // {
            //     Console.WriteLine(e);
            //     throw;
            // }
        }

        protected Simulation(SimulationParameters parameters)
        {
            Parameters = parameters;
            if (parameters.AreStartupParameters) State = SimulationState.Waiting;
        }

        public async Task SimulationTask()
        {
            if (State != SimulationState.WaitingForInitialization)
            {
                Stopwatch sw = new Stopwatch();
                State = SimulationState.Busy;
                Logger.Log("SimulationTask started....");

                while (Ticks < (Parameters.T / Parameters.TickMultiplier) &&
                       (State == SimulationState.Busy || State == SimulationState.Paused))
                {
                    if (State != SimulationState.Paused)
                    {
                        // RealTime = RealTime.AddHours(Parameters.TickRate);
                        RealTime = RealTime.AddHours(Parameters.TickMultiplier);
                        sw.Start();

                        // loop through the simulators and call the execute method
                        var tasks = new List<Task>();

                        foreach (AbstractSimulator sim in SimulatorRunners)
                        {
                            tasks.Add(Task.Run(() =>
                            {
                                try
                                {
                                    sim.Execute();
                                }
                                catch (Exception e)
                                {
                                    // Log Exception with reference to the simulator that triggered the global exception
                                    Logger.Log(e, sim);
                                    throw;
                                }
                            }));
                        }

                        await Task.WhenAll(tasks.ToArray()).ContinueWith(t =>
                        {
                            sw.Stop();
                            double lastTickDuration = sw.Elapsed.TotalMilliseconds;
                            Logger.Log($"Tick no. {Ticks} done in {lastTickDuration} ms ");
                            Ticks++;
                            sw.Reset();

                            if (Ticks % 5 == 0 || Ticks < 10)
                            {
                                Task.Run(async () =>
                                {
                                    try
                                    {
                                            Logger.Log($"Uploading metrics snapshot to database");
                                            Metric metric = new Metric(Ticks, lastTickDuration);
                                            await Socket.MetricsUpdate(metric);
                                            
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e);
                                        throw;
                                    }
                                });
                            }

                            // TODO: Way of cleaning up the tasks that have ran 
                        });
                    }
                }

                if (Parameters.T == Ticks)
                {
                    State = SimulationState.Finished;
                }
                else
                {
                    State = SimulationState.Waiting;
                }

                // Logger.Log($"total partnerships: {ContactSimulator.TotalPartnerships}");
                Logger.Log("======================");
                Logger.Log("Simulation now at status " + State.ToString());
                Logger.Log("======================");
                if (State == SimulationState.Finished)
                {
                    Logger.Log("======================");
                    Logger.Log("=DONEDONEDONEDONEDONE=");
                    Logger.Log("======================");
                    Logger.Log("======================");
                }

                if (Parameters.Looping)
                {
                    Logger.Log("--- Waiting 15 seconds for gestarting the simulation...");
                    await Task.Delay(20000);
                    Start();
                }

                // State = SimulationState.Finished;
            }
            else
            {
                Logger.Log("Simulation is not initalized, no task executed.");
            }
        }

        public void Start()
        {
            State = SimulationState.Busy;
            Ticks = 0;
            Task.Run(async () => { await SimulationTask(); }); // start new simulation in same thread
        }

        public void Stop()
        {
            Ticks = 0;
            State = SimulationState.Stopped;
        }

        public void Pause()
        {
            State = SimulationState.Paused;
        }

        public void ForceStop()
        {
            // TODO: Make SimulationTask Disposible by transfering it into a variable
            Stop();
            Ticks = Parameters.T;
        }

        public override string ToString()
        {
            return "=====     Simulation Instance     =====\n\n\n" +
                   "State: " + State.ToString() + "\n" +
                   "Population: " + Population + "\n" +
                   "Ticks: " + Ticks.ToString() + "\n" +
                   "Parameters: \n" + Parameters +
                   "=======================================\n\n\n";
        }

        public static void ResetForTesting()
        {
            _instance = null;
        }
    }

    public enum SimulationState
    {
        WaitingForInitialization,
        Waiting,
        Busy,
        Stopped,
        Exception,
        Finished,
        Paused
    }
}