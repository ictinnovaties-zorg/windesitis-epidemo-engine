﻿using Epidemo_Engine.Populator;

namespace Epidemo_Engine.RunTime
{
    public class SimulationMoq : Simulation
    {

        public SimulationMoq(SimulationParameters parameters) : base(parameters)
        {
        }
        
        public new SimulationState State { get; set; }
    }
}