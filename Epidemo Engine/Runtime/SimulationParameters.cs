using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using Epidemo_Engine.Exceptions;
using Epidemo_Engine.Infector;
using Epidemo_Engine.BehaviorPackage;
using Epidemo_Engine.DAL;

namespace Epidemo_Engine.RunTime
{
    public class SimulationParameters
    {
        public string SimulationName { get; internal set; }
        public int N { get; internal set; }
        public int T { get; internal set; }
        public Disease Disease { get; set; }

        public BehaviorPackage.Behavior Behavior { get; internal set; }

        // If TickRate = 1, a tick represents 1 hour.
        public double TickMultiplier
        {
            get => TickMultiplier1;
            set => TickMultiplier1 = value;
        }

        public DateTime StartTime { get; set; }
        public double DetectorPrevalence { get; internal set; }

        public DateTime RealTime { get; set; }
        public Random Random { get; internal set; }
        public bool Looping { get; set; }
        public bool AreStartupParameters = false;
        private double TickMultiplier1 = 1;

        /// <summary>
        /// Proper order of arguments: N, T, DiseaseId, BehaviourId, Tickrate/multiplier, StartTime, Detectorprevalence (optional), environment/simulationName [Optional || first one when there are no arguments supplied]
        /// </summary>
        /// <param name="args"></param>
        /// <exception cref="InputArgumentException"></exception>
        public SimulationParameters(string[] args)
        {
            StartTime = DateTime.Now;
            SimulationName = "local_" + Environment.MachineName + "_engine_runner";
            if (args.Length > 0)
            {
                if (args.Length < 2 && !Regex.IsMatch(args[0], @"^\d+$"))
                {
                    // parameterless startup with name
                    SimulationName = args[0] + "_" + Environment.MachineName + "_engine_runner";
                }
                else if (args.Length > 3)
                {
                    AreStartupParameters = true;
                    double detectorPrevalence = 1;
                    int diseaseId = 1;
                    int behaviorId = 1;
                    double tickRate = 1;
                    DateTimeOffset startTimeOffset = new DateTimeOffset();

                    try
                    {
                        if (args.Length > 2) diseaseId = Convert.ToInt32(args[2]);
                        if (args.Length > 3) behaviorId = Convert.ToInt32(args[3]);
                        if (args.Length > 4) tickRate = Convert.ToDouble(args[4]);
                        if (args.Length > 5)
                            startTimeOffset = DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(args[5]));
                        if (args.Length > 6) detectorPrevalence = Convert.ToDouble(args[6]);
                        if (args.Length > 7)
                            SimulationName = args[7] + "_" + Environment.MachineName + "_engine_runner";


                        Initialize(
                            Convert.ToInt32(args[0]),
                            Convert.ToInt32(args[1]),
                            diseaseId,
                            detectorPrevalence,
                            behaviorId,
                            tickRate,
                            startTimeOffset.UtcDateTime);
                    }
                    catch (FormatException e)
                    {
                        throw new InputArgumentException("Check the format of the input parameters.");
                    }
                }
                else
                {
                    throw new InputArgumentException("Too little arguments.");
                }
            }
        }

        public void InitializeJ(Dictionary<string, object> jsonObject)
        {
                this.Initialize(Convert.ToInt32(jsonObject["N"]), Convert.ToInt32(jsonObject["T"]),
                    Convert.ToInt32(jsonObject["DiseaseId"]),
                    Convert.ToDouble(jsonObject["DetectorPrevalence"]),
                    Convert.ToInt32(jsonObject["BehaviorId"]),
                    Convert.ToDouble(jsonObject["TickMultiplier"]),
                    Convert.ToDateTime(jsonObject["StartTime"])
                );
        }

        private void Initialize(int n, int t, int diseaseId, double detectorPrevalence, int behaviorId,
            double tickMultiplier,
            DateTime startTime)
        {
            N = n;
            T = t;

            Disease = ApiRepository.GetDiseaseById(diseaseId);
            Behavior = ApiRepository.GetBehaviourById(behaviorId);

            StartTime = startTime;
            RealTime = startTime;
            TickMultiplier = tickMultiplier;
            Disease.TickMultiplier = TickMultiplier;
            Random = new Random();
            DetectorPrevalence = detectorPrevalence;
        }

        public override string ToString()
        {
            return
                "   Name:    " + SimulationName.ToString() + "\n" +
                "   N:            " + N.ToString() + "\n" +
                "   T:            " + T.ToString() + "\n" +
                "   Disease:      " + ((Disease != null) ? "\n" + Disease.ToString() : "NOT AVAILABLE") + "\n" +
                "   Behavior:     " + ((Behavior != null) ? "\n" + Behavior.ToString() : "NOT AVAILABLE") + "\n" +
                "   TickMultiplier:     " + TickMultiplier.ToString() + "x\n" +
                "   StartTime:    " + StartTime.ToString() + "\n" +
                "   Detector Prevalence:    " + DetectorPrevalence.ToString() + "\n";
        }
    }
}