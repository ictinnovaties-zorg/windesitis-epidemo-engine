using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Epidemo_Engine.DAL;
using Newtonsoft.Json;
using SocketIOClient;

namespace Epidemo_Engine.RunTime
{
    public class SimulationSocket
    {
        private string Url = "http://localhost:3010/"; // running the Engine standalone / debugging mode
        // private string Url = "http://socketio_srv:3010/"; // running the Engine in a local docker instance
        //private string Url = "https://api.epidemio.run/";

        private SocketIO Socket;

        public async Task Connect()
        {
            Socket = new SocketIO(Url)
            {
                Parameters = new Dictionary<string, string>()
                {
                    {
                        "simulationId", Simulation.GetInstance().Id.ToString()
                    },
                    {"is_engine", "true"}
                },
                ConnectTimeout = TimeSpan.FromMilliseconds(5000)
            };

            try
            {
                await Socket.ConnectAsync();
                Logger.Log($"Socket Connected to {Url}");

                Socket.On("control",
                    res => HandleControlEvent(JsonConvert.DeserializeObject<Dictionary<string, object>>(res.Text)));
                Socket.On("parameters",
                    res => HandleParametersEvent(JsonConvert.DeserializeObject<Dictionary<string, object>>(res.Text)));
            }
            catch (Exception e)
            {
                Logger.Log("Could not connect to the SocketIO server within 2 sec.");
                // Logger.Log(e);
            }
        }

        private void HandleParametersEvent(Dictionary<string, object> obj)
        {
            Logger.Log("Processing incoming Parameters... " + JsonConvert.SerializeObject(obj));
            try
            {
                Simulation.GetInstance().Parameters.InitializeJ(obj);
                Simulation.GetInstance().Initialize();
                Simulation.GetInstance().InitializePopulation();
                Simulation.GetInstance().State = SimulationState.Waiting;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void HandleControlEvent(IReadOnlyDictionary<string, object> obj)
        {
            if (obj.ContainsKey("state"))
            {
                Simulation.GetInstance().State = (SimulationState) Convert.ToInt32(obj["state"].ToString());
                Logger.Log("Socket changed state tot " + Simulation.GetInstance().State);
            }


            if (obj.ContainsKey("start"))
            {
                Logger.Log("Start signal received, checking if current state is correct.");
                if (Simulation.GetInstance().State == SimulationState.Waiting ||
                    Simulation.GetInstance().State == SimulationState.Finished ||
                    Simulation.GetInstance().State == SimulationState.Stopped ||
                    Simulation.GetInstance().State == SimulationState.Exception)
                {
                    Simulation.GetInstance().Start();
                }
                else if (Simulation.GetInstance().State == SimulationState.Paused)
                {
                    Simulation.GetInstance().State = SimulationState.Busy;
                }
            }
        }

        public async Task SimulationEvent(string eventName, object eventPayload)
        {
            await Socket.EmitAsync("simulation_event", new
            {
                @event = eventName,
                data = eventPayload
            });
        }

        public async Task MetricsUpdate(Metric metric)
        {
            await Socket.EmitAsync("metric_update", metric);
        }
    }
}