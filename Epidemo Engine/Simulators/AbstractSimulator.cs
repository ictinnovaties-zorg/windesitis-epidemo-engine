using System.Threading;
using System.Threading.Tasks;
using Epidemo_Engine.RunTime;
using static Epidemo_Engine.Logger;

namespace Epidemo_Engine.Simulators
{
    public abstract class AbstractSimulator : ISimulator
    {
        public override string ToString()
        {
            return this.GetType().Name + " is living on Thread " + Thread.CurrentThread.ManagedThreadId.ToString();
        }

        public abstract void Execute();

        public Simulation Simulation { get; protected set; }

        protected void Log(object message, LogItemType logItemType = LogItemType.General)
        {
            Logger.Log(message,logItemType, this);
        }
    }
}