﻿using System;
using System.Diagnostics;
using Epidemo_Engine.DetectorPackage;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine.Simulators
{
    public class BehaviorSimulator : AbstractSimulator
    {
        public BehaviorSimulator() : this(Simulation.GetInstance()) {}

        public BehaviorSimulator(Simulation sim)
        {
            Simulation = sim;
        }

        private void Behavior()
        {
            lock (Simulation.Population)
            {
                for (var i = 0; i < Simulation.Population.Persons.Count; i++)
                {
                    var currentPerson = Simulation.Population.Persons[i];
                    if (IsDead(currentPerson)) continue;
                    SetupPercentages(currentPerson);
                    Behave(currentPerson);
                }
            }
        }

        private void SetupPercentages(Person p)
        {
            SetContactAvoidance(p);
            SetVaccinationPercentage(p);
            SetSeeDoctorOnSymptomsPercentage(p);
        }

        public void SetContactAvoidance(Person p)
        {
            if (HasDetector(p))
            {
                p.Behavior.ContactAvoidanceOnSymptoms = p.SuspectedState switch
                {
                    SuspectedState.PossibleCarrier => 0.40,
                    SuspectedState.AtRisk => 0.70,
                    SuspectedState.ConfirmedCarrier => 0.80,
                    _ => 0
                };
            }
            else
                p.Behavior.ContactAvoidanceOnSymptoms = HasSymptoms(p) ? 0.60 : p.ContactProperties.PfOriginal;
        }

        public void SetVaccinationPercentage(Person p)
        {
            if (HasDetector(p))
            {
                p.Behavior.VaccinationOnSymptoms = p.SuspectedState switch
                {
                    SuspectedState.PossibleCarrier => 0.30,
                    SuspectedState.AtRisk => 0.50,
                    SuspectedState.ConfirmedCarrier => 0.80,
                    _ => 0
                };
            }
        }

        public void SetSeeDoctorOnSymptomsPercentage(Person p)
        {
            if (HasDetector(p))
            {
                p.Behavior.SeeDoctorOnSymptoms = p.SuspectedState switch
                {
                    SuspectedState.PossibleCarrier => 0.40,
                    SuspectedState.AtRisk => 0.70,
                    SuspectedState.ConfirmedCarrier => 0.80,
                    _ => 0.00
                };
            }
        }

        public void AvoidContact(Person p)
        {
            p.ContactProperties.Pf *= p.Behavior.ContactAvoidanceOnSymptoms;
        }

        private void Vaccinate(Person p)
        {
            p.State = Simulation.Parameters.Disease.Compartments.Contains(PersonState.Immune)
                ? PersonState.Immune
                : PersonState.Susceptible;
            
            p.SuspectedState = SuspectedState.Safe;
            p.ContactProperties.Pf = p.ContactProperties.PfOriginal;
        }

        private void SeeDoctor(Person p)
        {
            if (TruePositive(p))
            {
                p.SuspectedState = HasDetector(p) ? SuspectedState.ConfirmedCarrier : SuspectedState.Safe;
                if (!ShouldVaccinate(p)) return;
                Vaccinate(p);
            }
            else if (!FalseNegative(p))
            {
                p.SuspectedState = HasDetector(p) ? SuspectedState.Safe : p.SuspectedState;
                p.ContactProperties.Pf = p.ContactProperties.PfOriginal;
            }
        }

        public void Behave(Person p)
        {
            if (!HasSymptoms(p) && !HasRiskyState(p)) return;
            if (!HasDetector(p) && !HasSymptoms(p)) return;
            if (!HasRiskyState(p) && HasDetector(p)) p.SuspectedState = SuspectedState.PossibleCarrier;

            if (WillAvoidContact(p)) AvoidContact(p);
            if (!SeesDoctorOnSymptoms(p)) return;

            SeeDoctor(p);
        }

        private bool HasRiskyState(Person p) => p.SuspectedState == SuspectedState.AtRisk ||
                                                p.SuspectedState == SuspectedState.PossibleCarrier;

        private bool HasSymptoms(Person p) => p.State == PersonState.Infected;
        private bool IsDead(Person p) => p.State == PersonState.Dead;
        private bool HasDetector(Person p) => p.HasDetector;

        private bool SeesDoctorOnSymptoms(Person p) =>
            Simulation.Parameters.Random.Next(0, 100) < p.Behavior.SeeDoctorOnSymptoms * 100;

        private bool WillAvoidContact(Person p) =>
            Simulation.Parameters.Random.Next(0, 100) < p.Behavior.ContactAvoidanceOnSymptoms * 100;

        private bool ShouldVaccinate(Person p) =>
            Simulation.Parameters.Random.Next(100) < p.Behavior.VaccinationOnSymptoms * 100;

        private bool TruePositive(Person p) =>
            Simulation.Parameters.Random.Next(0, 100) < p.Behavior.DiagnosisSpecificity * 100;

        private bool FalseNegative(Person p) =>
            Simulation.Parameters.Random.Next(0, 100) < p.Behavior.DiagnosisSensitivity * 100;

        public override void Execute()
        {
            var sw = new Stopwatch();
            sw.Start();

            Behavior();
            sw.Stop();

            Log($"BehaviorSimulator took: {Math.Round(sw.Elapsed.TotalMilliseconds, 2)} ms");
        }
    }
}