using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;
using Epidemo_Engine.Populator;
using System.Timers;
using Epidemo_Engine.RunTime;
using Epidemo_Engine.Simulators;

namespace Epidemo_Engine
{
    public class ContactSimulator : AbstractSimulator
    {
        public static int TotalPartnerships { get; private set; }

        public ContactSimulator() : this(Simulation.GetInstance()) {}

        public ContactSimulator(Simulation sim)
        {
            Simulation = sim;
        }

        private DateTime _doneCreatingPartnerships;
        private DateTime _startedCreatingPartnerships;

        /// <summary>
        /// Creates new partnerships based on ContactProperties
        /// </summary>
        /// <returns></returns>
        private void CreatePartnerships()
        {
            Log("CreatePartnerships started...");
            if (_startedCreatingPartnerships == default(DateTime)) _startedCreatingPartnerships = DateTime.Now;

            Stopwatch sw = new Stopwatch();
            int timesRm = 0;
            int timesFfm = 0;
            int timesFfmAttempted = 0;
            int timesNeighborhoodEmpty = 0;

            int startpoint = 0;
            int endpoint = Simulation.Population.Persons.Count;
            sw.Start();
        
            // Start parallel for loop that makes multiple threads for looping through the Population
            Parallel.For(startpoint, endpoint, i =>
            {
                Person currentPerson;
                lock (Simulation.Population)
                {
                    currentPerson = Simulation.Population.Persons[i];

                    if (currentPerson.Neighborhood.Count >= currentPerson.ContactProperties.Mp) return;

                    if (!(Simulation.Parameters.Random.Next(100) < (100 * currentPerson.ContactProperties.Pf))) return;

                    if (Simulation.Parameters.Random.Next(100) < (100 * currentPerson.ContactProperties.Pr))
                    {
                        Rm(currentPerson);
                        timesRm++;
                        return;
                    }

                    // Use FFM to create new partnership
                    timesFfmAttempted++;
                    // List of all the friends of friends of the individual (if they exist)
                    if (currentPerson.Neighborhood.Count > 1)
                    {
                        bool ffmResult = FFM(currentPerson);
                        if (ffmResult)
                        {
                            timesFfm++;
                            return;
                        }
                    }
                    else
                    {
                        timesNeighborhoodEmpty++;
                    }

                    // FFM unsuccessful, use RM
                    Rm(Simulation.Population.Persons[i]);
                    timesRm++;
                }
            });
            sw.Stop();

            double ffmSuccess = Math.Round((double) timesFfm / timesFfmAttempted * 100, 2);
            double ffmNeighborhoodEmpty = Math.Round((timesNeighborhoodEmpty / (double) timesFfmAttempted) * 100, 2);

            if (timesRm + timesFfm == 0)
            {
                if (_doneCreatingPartnerships == default(DateTime)) _doneCreatingPartnerships = DateTime.Now;
                Log(
                    $"All Partnerships done within {(_doneCreatingPartnerships - _startedCreatingPartnerships).TotalMilliseconds} ms");
            }

            TotalPartnerships += (timesRm + timesFfm);

            Log(
                $"Created {timesRm + timesFfm} partnerships, FFM/RM Ratio {timesFfm}/{timesRm}, (took {Math.Round(sw.Elapsed.TotalMilliseconds, 2)} ms). FFM attempted {timesFfmAttempted} times (+{timesFfmAttempted - timesFfm}). (success: {ffmSuccess}% neighborhood empty: {ffmNeighborhoodEmpty}%). ");
        }


        /// <summary>
        /// Friends-of-Friends mixing for passed person
        /// </summary>
        /// <param name="currentPerson"></param>
        /// <returns></returns>
        private bool FFM(Person currentPerson)
        {
            Person p1 = null;
            Person potential = null;
            int attempts = 0;
            do
            {
                p1 = currentPerson.Neighborhood[Simulation.Parameters.Random.Next(currentPerson.Neighborhood.Count)];

                int n = Simulation.Parameters.Random.Next(p1.Neighborhood.Count);

                potential = p1.Neighborhood[n];
                attempts++;
            } while ((potential.Neighborhood.Count >= currentPerson.ContactProperties.Mp ||
                      potential.Neighborhood.Contains(currentPerson)) && attempts < 100);

            if (attempts <= 100)
            {
                currentPerson.Neighborhood.Add(potential);
                potential.Neighborhood.Add(currentPerson);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Random mixing for person
        /// </summary>
        /// <param name="person"></param>
        private bool Rm(Person person)
        {
            Person p = Simulation.Population.Persons[
                Simulation.Parameters.Random.Next(Simulation.Population.Persons.Count)];

            int attempts = 0;
            while (p.Neighborhood.Count >= p.ContactProperties.Mp || p.Id ==
                                                         person.Id
                                                         || person.Neighborhood.Contains(p)
                                                         || p.Neighborhood.Contains(person)
                                                         && attempts < 100)
            {
                attempts++;
                p = Simulation.Population.Persons[
                    Simulation.Parameters.Random.Next(Simulation.Population.Persons.Count)];
            }

            if (attempts >= 100) return false;
            person.Neighborhood.Add(p);
            p.Neighborhood.Add(person);
            return true;
        }

        /// <summary>
        /// Break a partnership 
        /// </summary>
        public void BreakPartnerships()
        {
            int countBrokenPartnerShips = 0;
            Parallel.For(0, Simulation.Population.Persons.Count, i =>
            {
                lock (Simulation.Population)
                {
                    Person currentPerson = Simulation.Population.Persons[i];

                    // Check if Neighborhood contains enough relations
                    if (!(currentPerson.Neighborhood.Count >= 1)) return;

                    // probability (Pb) that relation will be broken
                    if (!(Simulation.Parameters.Random.Next(100) <= currentPerson.ContactProperties.Pb * 100)) return;

                    countBrokenPartnerShips++;

                    int index = Simulation.Parameters.Random.Next(currentPerson.Neighborhood.Count);

                    Person randomPerson = currentPerson.Neighborhood[index];
                    
                    // Remove randomPerson from Neighborhood
                    currentPerson.Neighborhood.Remove(randomPerson);

                    // Remove person from Neighborhood from randomPerson
                    randomPerson.Neighborhood.Remove(currentPerson);
                }
            });

//            Log($"nr persons: {Simulation.Population.Persons.Count}");
            Log($"nr of persons broken partnership  {countBrokenPartnerShips}");
        }

        public void SetTickMultiplier(int multiplier)
        {
        }

        public override void Execute()
        {
            Log(this);
            CreatePartnerships();
            BreakPartnerships();
        }
    }
}