﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Epidemo_Engine.DetectorPackage;
using Epidemo_Engine.Infector;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Epidemo_Engine.Simulators;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.EntityFrameworkCore.SqlServer.Internal;

namespace Epidemo_Engine.Simulators
{
    public class DetectorSimulator : AbstractSimulator
    {
        public DetectorSimulator() : this(Simulation.GetInstance()){}

        public DetectorSimulator(Simulation sim)
        {
            Simulation = sim;
        }
        
        /// <summary>
        /// Updates the SuspectedState of the whole population for the current time interval.
        /// </summary>
        public void UpdateSuspectedStates()
        {
            int detectorUserCount = 0;
            List<Person> usersToSafe = new List<Person>();
            List<Person> usersToAtRisk = new List<Person>();
            List<Person> usersToPossibleCarrier = new List<Person>();
            List<Person> usersToConfirmedCarrier = new List<Person>();
            
            // Looking for SuspectedStates that need to be changed.
            Parallel.For(0, Simulation.Population.Persons.Count, i =>
            {
                lock (Simulation.Population)
                {
                    Person p = Simulation.Population.Persons[i];
                    
                    // Detector only has knowledge of Person with a Detector.
                    if (!p.HasDetector) return;
                    
                    detectorUserCount++;
                        
                    switch (p.SuspectedState)
                    {
                        case SuspectedState.Safe:
                            usersToSafe.Add(p);
                            break;
                        case SuspectedState.AtRisk:
                            usersToAtRisk.Add(p);
                            // todo: Check likelihood that the subject has been affected at the current time. (increases over time).
                            break;
                        case SuspectedState.PossibleCarrier:
                            // Search through neighbors.
                            List<Person> possibleNeighbors =
                                FindNeighborsWithState(p, SuspectedState.PossibleCarrier);
                            List<Person> confirmedNeighbors =
                                FindNeighborsWithState(p, SuspectedState.ConfirmedCarrier);
                            if (possibleNeighbors.Count > 0)
                            {
                                // If there are more possible carriers than the user, all possible carriers are most likely a carrier.
                                usersToConfirmedCarrier.Add(p);
                                usersToAtRisk.AddRange(possibleNeighbors);
                                // All contacts who are not a carrier are now at a possible risk of infection.
                                usersToAtRisk.AddRange(FindNeighborsWithState(p, SuspectedState.Safe));
                            }
                            else if (confirmedNeighbors.Count > 0)
                            {
                                // If there are more possible carriers than the user, all possible carriers are most likely a carrier.
                                usersToConfirmedCarrier.Add(p);
                                // All contacts who are not a carrier are now at a possible risk of infection.
                                usersToAtRisk.AddRange(FindNeighborsWithState(p, SuspectedState.Safe));
                            }
                            else
                            {
                                // Otherwise person stays possibleCarrier.
                                usersToPossibleCarrier.Add(p);
                            }

                            break;
                        case SuspectedState.ConfirmedCarrier:
                            usersToConfirmedCarrier.Add(p);
                            usersToAtRisk.AddRange(FindNeighborsWithState(p, SuspectedState.Safe));
                            // todo: Calculate likelihood that the subject 's disease has incubated.
                            // todo: Calculate likelihood that the subject is infectious.
                            // todo: Calculate likelihood that the subject passes away next day.
                            // todo: Calculate likelihood that the subject has recovered from the disease naturally.
                            break;
                    }
                }
            }); // End of Parallel For loop.
            
            //Changing SuspectedState for every group.
            ChangeGroupStateTo(usersToSafe, SuspectedState.Safe);
            ChangeGroupStateTo(usersToAtRisk, SuspectedState.AtRisk);
            ChangeGroupStateTo(usersToPossibleCarrier, SuspectedState.PossibleCarrier);
            ChangeGroupStateTo(usersToConfirmedCarrier, SuspectedState.ConfirmedCarrier);
            
            Log($"Detector users =  {detectorUserCount}, population suspected states: \n" +
                $"    Safe = {usersToSafe.Count}, AtRisk = {usersToAtRisk.Count}, PossibleCarriers = {usersToPossibleCarrier.Count}, ConfirmedCarriers = {usersToConfirmedCarrier.Count}");
        }

        /// <summary>
        /// Returns a list of neighbors of the specified individual, which have the specified state. 
        /// </summary>
        /// <param name="p">Person who's neighbors should be checked for state x.</param>
        /// <param name="state">Looking for this state in neighbors.</param>
        /// <returns></returns>
        private List<Person> FindNeighborsWithState(Person p, SuspectedState state)
        {
            // Detector only has knowledge of Person with a Detector
            return p.Neighborhood.Where(person => person.HasDetector && person.SuspectedState == state).ToList();
        }
        
        /// <summary>
        /// Change state of the selected group.
        /// </summary>
        /// <param name="pList">List of neighbors to be changed.</param>
        /// <param name="state">New state for neighbors.</param>
        /// <returns></returns>
        private int ChangeGroupStateTo(List<Person> pList, SuspectedState state)
        {
            lock (Simulation.Population)
            {
                for (int i = 0; i < pList.Count; i++)
                {
                    // Detector only has knowledge of Person with a Detector
                    if (pList[i].HasDetector)
                    {
                        pList[i].SuspectedState = state;
                    }
                }
            }

            return pList.Count; // Return amount of persons who's state has been changed.
        }
        
        
        /// <summary>
        /// DetectorSimulation executed methods and functions during every time interval. (time interval  = tick)
        /// </summary>
        public override void Execute()
        {
            UpdateSuspectedStates();
        }
    }
}