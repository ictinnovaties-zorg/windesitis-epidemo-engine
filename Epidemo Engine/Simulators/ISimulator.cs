﻿using System.Threading.Tasks;
using System.Timers;
using Epidemo_Engine.RunTime;

namespace Epidemo_Engine
{
    public interface ISimulator
    {
        void Execute();
        
        Simulation Simulation { get; }
    }
}