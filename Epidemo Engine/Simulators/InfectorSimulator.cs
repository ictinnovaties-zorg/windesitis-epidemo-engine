﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Epidemo_Engine.Infector;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Epidemo_Engine.Simulators;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace Epidemo_Engine
{
    public class InfectorSimulator : AbstractSimulator
    {
        public Disease Disease { get; set; }
        public InfectorSimulator() : this(Simulation.GetInstance()) {}
        public InfectorSimulator(Simulation sim)
        {
            Simulation = sim;
            Disease = Simulation.Parameters.Disease;
        }

        public void Infector()
        {
            int amountInfected = 0;
            lock (Simulation.Population)
            {
                List<Person> infectiousPersons = Simulation.Population.GetInfectiousPersons();

                if (infectiousPersons.Count == 0) return;

                Parallel.For(0, infectiousPersons.Count, i =>
                {
                    Person infectedPerson = infectiousPersons[i];
                    List<Person> neighborhood = infectedPerson.Neighborhood;

                    if (neighborhood.Count == 0) return;

                    for (int j = 0; j < neighborhood.Count; j++)
                    {
                        Person neighbor = neighborhood[j];
                        if (infectedPerson.InfectionDate?.Date == Simulation.RealTime.Date ||
                            neighbor.State != PersonState.Susceptible) continue;

                        if (TryInfect(neighbor))
                            amountInfected++;
                    }
                });
            }

            Log(
                $"Infected by infector: {amountInfected}");
        }

        public bool TryInfect(Person neighbor)
        {
            if (!ShouldInfect()) return false;
            
            neighbor.InfectionDate = Simulation.RealTime;
            neighbor.RecoveryDate = null;
            neighbor.State = Disease.Compartments.Contains(PersonState.Exposed)
                ? PersonState.Exposed
                : PersonState.Infected;
            return true;
        }

        public bool ShouldInfect() => Simulation.Parameters.Random.Next(100) < (100* Disease.InfectionChance);

        public override void Execute()
        {
            Log(this);
            Infector();
        }
    }
}