﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Epidemo_Engine.DAL;
using Epidemo_Engine.Populator;
using Epidemo_Engine.RunTime;
using Epidemo_Engine.Simulators;

namespace Epidemo_Engine.Populator
{
    public class PopulationSimulator : AbstractSimulator
    {
        private PopulationProperties Properties { get; }
        private int _amountRecovered = 0;
        private int _amountDied = 0;

        public PopulationSimulator(PopulationProperties properties) : this()
        {
            Properties = properties;
        }

        private PopulationSimulator() : this(Simulation.GetInstance()) {}

        // For testing
        public PopulationSimulator(Simulation sim)
        {
            Simulation = sim;
            Simulation.Population = new Population(sim);
        }

        public void GeneratePopulation()
        {
            // var db = new SimulationContext();
            // db.Database.EnsureCreated();
            try
            {
                // db.Populations.Add(Simulation.Population);
                // db.SaveChanges();

                for (int i = 0; i < Properties.N; i++)
                {
                    bool hasDetector = Simulation.Parameters.Random.Next(0,100) <= (Simulation.Parameters.DetectorPrevalence*100);
                    Simulation.Population.Persons.Add(new Person(i, PersonState.Susceptible, hasDetector));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        public void TransmitStates()
        {
            _amountDied = 0;
            _amountRecovered = 0;
            lock (Simulation.Population)
            {
                for (int i = 0; i < Simulation.Population.Persons.Count; i++)
                {
                    Person p = Simulation.Population.Persons[i];
                    TryModify(p);
                }
            }

            Log(
                $"Recovered {_amountRecovered}, Died {_amountDied}, total infected: {Simulation.Population.GetInfectiousPersons().Count}");
        }

        private bool TryModify(Person p)
        {
            if (TrySuccumb(p)) return true;
            
            switch (p.State)
            {
                case PersonState.Immune:
                    return false;
                case PersonState.Exposed when IsIncubationPeriodOver(p):
                    return IncubationPeriodOver(p);
                case PersonState.Infected when ShouldRecover():
                    InfectionPeriodOver(p);
                    _amountRecovered++;
                    break;
                case PersonState.Infected when ShouldDie():
                    _amountDied++;
                    return KillByDisease(p);
                case PersonState.Recovered when IsRecoveryPeriodOver(p):
                    return RecoveryPeriodOver(p);
                default:
                    return false;
            }

            return false;
        }

        // Die due to infection.
        private bool KillByDisease(Person p)
        {
            p.State = PersonState.Dead;
            return true;
        }

        // Die due to old age.
        private bool TrySuccumb(Person p)
        {
            if (!IsMaxAgeReached(p)) return false;
            p.State = PersonState.Dead;
            _amountDied++;
            return true;
        }

        private bool InfectionPeriodOver(Person p)
        {
            p.RecoveryDate = Simulation.RealTime.Date;
            p.InfectionDate = null;
            p.State = Simulation.Parameters.Disease.Compartments.Contains(PersonState.Recovered)
                ? PersonState.Recovered
                : PersonState.Susceptible;
            return true;
        }

        private bool RecoveryPeriodOver(Person p)
        {
            if (!ShouldRecover()) return false;
            
            p.RecoveryDate = null;
            p.State = Simulation.Parameters.Disease.ImmuneAfterRecovery &&
                      Simulation.Parameters.Disease.Compartments.Contains(PersonState.Immune)
                ? PersonState.Immune
                : PersonState.Susceptible;
            p.ContactProperties.Pf = p.ContactProperties.PfOriginal;
            return true;
        }

        private bool IncubationPeriodOver(Person p)
        {
            p.State = PersonState.Infected;
            return true;
        }

        private bool ShouldDie() =>
            Simulation.Parameters.Random.Next(100) < (100 * Simulation.Parameters.Disease.FatalityChance);
        private bool ShouldRecover() =>
            Simulation.Parameters.Random.Next(100) < (100 * Simulation.Parameters.Disease.RecoveryChance);
        private bool IsIncubationPeriodOver(Person p) => Simulation.RealTime >= p.InfectionDate?.AddDays(Simulation.Parameters.Disease.IncubationPeriod);
        private bool IsRecoveryPeriodOver(Person p) => Simulation.RealTime >= p.RecoveryDate?.AddDays(Simulation.Parameters.Disease.RecoveryPeriod);
        private bool IsMaxAgeReached(Person p) => p.BirthDate != null && (Simulation.RealTime - p.BirthDate).Value.Days / 365 - 1 >= 80;

        public override void Execute()
        {
            var round = (int)Math.Round((double)(100 * Simulation.Population.GetInfectiousPersons().Count) / Simulation.Population.Persons.Count);
            TransmitStates();
            Log(
                $"susceptible: {Simulation.Population.GetSusceptiblePersons().Count} | " +
                $"exposed: {Simulation.Population.GetExposedPersons().Count} | " +
                $"infected: {Simulation.Population.GetInfectedPersons().Count} | " +
                $"Recovered: {Simulation.Population.GetRecoveredPersons().Count} | " +
                $"Immune: {Simulation.Population.GetImmunePersons().Count} | " +
                $"Dead: {Simulation.Population.GetDeadPersons().Count} | " +
                $"Total infected: {round}%");

            Log(this);
        }
    }
}