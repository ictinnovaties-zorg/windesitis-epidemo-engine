EPIDEMIO ENGINE PROJECT
=====

### Requirements
This .net core project requires .net core 3.1. 
To run this project within a (pre-)production environment you need to use Docker.
 

### Simulation Start options

**Without parameters** 

The simulation will wait on socket commando's to fill parameters and start the simulation engines (from the front->back-end->socket-io server)

**With Parameters**

The simulation will start right away with the configured parameters as described in Runtime/SimulationParameters.cs


### Docker Runner Guide

For testing the simulation in a production environment, use Docker.

**Option 1:** Make use of the pre-configured run-configurations for Jetbrains Rider

**Option 2:** Run docker commands yourself


#### Build Engine Docker image

```bash
docker build -t windesheim_epidemo_engine_image .
```

#### Docker Production Engine Compose 

```bash
cd "Epidemio Engine"
docker-compose up -d
# provide --scale epidemo_engine=xxx arguments when testing scaling 
```


### Deployment
Run Deployment/deploy.sh with as only required argument the bitbucket project branch. Check if your current (server) user has the correct permissions to execute the script (you may have to add custom user to docker group).

### Change API Repository to match your environment

The pre-configured and hosted Epidemio Back-end API on https://api.epidemio.run will be discontinued after 15-02-2020. 
To make use of your self-hosted, or docker hosted Epidemio Back-end API, change the api_url variable accordingly in DAL\ApiRepository.cs and Runtime\SimulationSocket.cs:

```c#
// private static string api_url = "http://localhost:801/api/"; 
// private static string api_url = "http://localhost:5000/api/"; 
private static string api_url = "https://api.epidemio.run/api/";

```  

