using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ResearchDataStructure
{
    public class LoggerContext : DbContext
    {
        public DbSet<TestingLogItem> TestingLogItems { get; set; }
        public static readonly ILoggerFactory MyLoggerFactory
            = LoggerFactory.Create(builder => { builder.AddConsole(); });
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=localhost;Database=PopulationDB;User=sa;Password=Research123!");
            optionsBuilder.UseLoggerFactory(MyLoggerFactory).EnableSensitiveDataLogging();
        }
    }
}