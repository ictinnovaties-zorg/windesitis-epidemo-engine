using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ResearchDataStructure
{
    public class PerformanceTestRunner
    {
        private List<PopulationItem> Population; // The population
        private int N = 1000;
        private int Mp = 10; // Maximum number of partners per person
        private double Pf = 1; // Initial probability of forming a new partnership, gets mutated every tick
        private double Pr = 0.95; // Probability that Random Mixing is used to create a new partnership
        private double dPf = 1; // Rate at which Pf changes, Pf is multiplied by this value every tick
        private int timesteps = 10; // Total number of timesteps
        private static readonly Random random = new Random();

        public PerformanceTestRunner()
        {
            this.Population = new List<PopulationItem>();
        }

        public void Start()
        {
            GeneratePopulation();
            CreatePartnerships();
            AverageGlobalClustering(1000);
//            CCC(Population.First(p => p.neighborhood.Count == Population.Max(p1 => p1.neighborhood.Count)));

        }

        public void GeneratePopulation()
        {
            var rnd = new Random();
            // Fill population list with new PopulationItems (people)
            for (int i = 0; i < N; i++)
                this.Population.Add(new PopulationItem(i,
                    (PopulationStatus) Enum.GetValues(typeof(PopulationStatus)).GetValue((rnd.Next(0, 3)))));
        }

        /**
         * Generates partnerships in the population.
         */
        public void CreatePartnerships()
        {
            // Get list of all partners of all partners of the individual
            Console.WriteLine($"Creating parterships with N:{N}, Pf:{Pf}, Pr:{Pr}, Mp:{Mp}, timesteps: {timesteps}");
            var rnd = new Random();
            Stopwatch sw = new Stopwatch();
            int timesRM = 0;
            int timesFFM = 0;
            int timesFFMAttempted = 0;
            int timesNeighborhoodEmpty = 0;
            sw.Start();

            for (var t = 0; t < timesteps; t++)
            {
                for (var i = 0; i < Population.Count; i++)
                {
                    // If the person has less than the maximum number of partners, continue
                    if (Population[i].neighborhood.Count >= Mp) continue;

                    // Form a new partnership with probability Pf
                    if (!(rnd.Next(100) < (100 * Pf))) continue;

                    // Decide if RM will be used based on probability Pr
                    if (rnd.Next(100) < (100 * Pr))
                    {
                        // Use RM to create a new partnership
                        RM(Population[i]);
                        timesRM++;
                        continue;
                    }

                    // Use FFM to create new partnership
                    timesFFMAttempted++;
                    // List of all the friends of friends of the individual (if they exist)
                    if (Population[i].neighborhood.Count > 1)
                    {
                        // Populate Friends-of-Friends list
                        List<PopulationItem> FoF = new List<PopulationItem>();

                        for (var i1 = 0; i1 < Population[i].neighborhood.Count; i1++)
                        {
                            for (var i2 = 0; i2 < Population[i].neighborhood[i1].neighborhood.Count; i2++)
                            {
                                // If the friend-of-friend is not the individual itself 
                                if (Population[i].neighborhood[i1].neighborhood[i2].PopulationItemId ==
                                    Population[i].PopulationItemId) continue;

                                // If the friend-of-friend is not already in the FoF list 
                                if (!FoF.Contains(Population[i].neighborhood[i1].neighborhood[i2]))
                                    FoF.Add(Population[i].neighborhood[i1].neighborhood[i2]);
                            }
                        }

                        // Remove the individual itself from the list if it happens to be on there (shouldn't be possible)
                        FoF.Remove(Population[i]);

                        if (FoF.Count > 0)
                        {
                            // Randomly select a new partner from the FoF list that has less than Mp partners
                            PopulationItem partner = FoF[rnd.Next(FoF.Count)];
                            int attempts = 0;
                            while (partner.neighborhood.Count >= Mp & attempts < 100)
                            {
                                partner = FoF[rnd.Next(FoF.Count)];
                                attempts++;
                            }

                            if (attempts >= 100) continue;

                            // Create the symmetrical partnership
                            Population[i].neighborhood.Add(partner);
                            partner.neighborhood.Add(Population[i]);
                            timesFFM++;
                        }
                    }
                    else
                    {
                        timesNeighborhoodEmpty++;
                    }

                    // FFM unsuccessful, use RM
                    RM(Population[i]);
                }
            }

            sw.Stop();

            Console.WriteLine(
                $"Generated {timesRM + timesFFM} partnerships in {Math.Round(sw.Elapsed.TotalMilliseconds, 2)} milliseconds.");
            Console.WriteLine(
                $"FFM attempted {timesFFMAttempted} times, succeeded {timesFFM} times ({Math.Round((double) timesFFM / timesFFMAttempted * 100, 2)}% of the time). RM used {timesRM} times.");
            Console.WriteLine(
                $"During FFM, an individuals neighborhood was empty (or 1): {timesNeighborhoodEmpty} times  ({Math.Round((timesNeighborhoodEmpty / (double) timesFFMAttempted) * 100, 2)}% of the time).");
        }


        // Randomly select a new partner that has less than Mp partners, is not the individual itself and does not already have a partnership with the individual
        public void RM(PopulationItem person)
        {
            PopulationItem p = Population[random.Next(N)];
            int attempts = 0;
            while (p.neighborhood.Count >= Mp || p.PopulationItemId ==
                                              person.PopulationItemId
                                              || person.neighborhood.Contains(p)
                                              || p.neighborhood.Contains(person)
                                              && attempts < 100)
            {
                attempts++;
                p = Population[random.Next(N)];
            }

            if (attempts >= 100) return;

            person.neighborhood.Add(p);
            p.neighborhood.Add(person);
        }

        /**
         * Calculates the local clustering coefficient for the passed individual
         */
        public void LocalClusteringCoefficient(PopulationItem v)
        {
            var Ni = v.neighborhood;
            int ki = Ni.Count;
            double max = (double) (ki * (ki - 1)) / 2;

            HashSet<ValueTuple<int, int>> links = new HashSet<(int, int)>();

            for (var i = 0; i < Ni.Count; i++)
            {
                for (var i1 = 0; i1 < Ni.Count; i1++)
                {
                    if (Ni[i].neighborhood.Contains(Ni[i1]))
                    {
                        links.Add((Ni[i].PopulationItemId, Ni[i1].PopulationItemId));
                        links.Add((Ni[i1].PopulationItemId, Ni[i].PopulationItemId));
                    }
                }
            }

            double actual = (double)links.Count/2;
            double Ci = actual / max;
            Console.WriteLine($"Ci : {Ci}");
        }

        // Not sure if this works properly
        // https://www.geeksforgeeks.org/clustering-coefficient-graph-theory/
        public void AverageGlobalClustering(int trials)
        {
            if (trials > Population.Count)
                trials = Population.Count;
            
            double triangles = 0;
            // Choose a random individual
            // Choose two of its neighbors at random and see if they are connected
            for (int i = 0; i < trials; i++)
            {
                if(Population[i].neighborhood.Count < 2)
                    continue;

                var p1 = Population[random.Next(Population.Count)];
                var p2 = Population[random.Next(Population.Count)];

                if (p1.neighborhood.Contains(p2))
                    triangles++;
            }

            double cc = triangles / trials;
            Console.WriteLine($"Average global clustering coefficient over {trials} trials: {cc}");
            
        }

        public void GetAverageNumberOfPartnerships()
        {
            double avg = Population.Select(p => p.neighborhood.Count).Average();
            double max = Population.Max(p => p.neighborhood.Count);

            Console.WriteLine($"Average number of partnerships: {avg}. Max number of partnerships: {max}");
        }

        public void GetNumberInfectedLINQ(int repetitions)
        {
            double totalMilliseconds = 0;
            int amountInfected = 0;

            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < repetitions; i++)
            {
                sw.Start();

                amountInfected = this.Population.Count(p => p.status == PopulationStatus.infected);

                sw.Stop();
                totalMilliseconds += sw.Elapsed.TotalMilliseconds;
                sw.Reset();
            }

            double averageMilliseconds = totalMilliseconds / repetitions;

            Console.WriteLine(
                $"Found {amountInfected} infected individuals. Took an average of {Math.Round(averageMilliseconds, 2)} milliseconds over {repetitions} repetitions");
        }

        public void GetAverageNumberInfectedInNeighborhood()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int max = Population.Max(p => p.neighborhood.Count);

            PopulationItem pmax = Population.First(p => p.neighborhood.Count == max);
            sw.Stop();

            Console.WriteLine(
                $"Took {Math.Round(sw.Elapsed.TotalMilliseconds, 2)} milliseconds to find the person with the highest amount of partnerships {max}.");
        }

        public void PrintPopulationSorted()
        {
            List<PopulationItem> sorted = Population.OrderBy(p => p.neighborhood.Count).ToList();
            foreach (var populationItem in sorted)
            {
                Console.WriteLine(
                    $"PopulationItem with Id: {populationItem.PopulationItemId} has {populationItem.neighborhood.Count} partnerships.");
            }
        }
    }
}