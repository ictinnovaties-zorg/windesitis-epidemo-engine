using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;

namespace ResearchDataStructure
{
    public class PopulationContext : DbContext
    {
        public DbSet<PopulationItem> PopulationItems { get; set; }
        

        public static readonly ILoggerFactory MyLoggerFactory
            = LoggerFactory.Create(builder => { builder.AddConsole(); });

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (Settings.cType == ConnectionType.mysql)
            {
                optionsBuilder.UseMySql("server=localhost;database=PopulationDB;user=root;password=Research123!");
            }
            else if (Settings.cType == ConnectionType.sql)
            {
                optionsBuilder.UseSqlServer(
                    @"Server=localhost;Database=PopulationDB;User=sa;Password=Research123!");
            }

            optionsBuilder.UseLoggerFactory(MyLoggerFactory).EnableSensitiveDataLogging();
        }
    }
}