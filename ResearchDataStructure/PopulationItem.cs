using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ResearchDataStructure
{
    public class PopulationItem
    {
        [Key]
        public int PopulationItemId { get; set; }
        public PopulationStatus status { get; set; }
        // TODO: Test linkedlist vs arraylist implementations of the adjacency list
        public List<PopulationItem> neighborhood { get; set; } // Adjacency list

        // This property is only for debugging purposes
        [NotMapped]
        public List<int> neighborhoodIds
        {
            get
            {
                List<int> ids = new List<int>();
                neighborhood.ForEach(p => ids.Add(p.PopulationItemId));
                return ids;
            }
        }

        public PopulationItem(int populationItemId, PopulationStatus status)
        {
            this.PopulationItemId = populationItemId;
            this.status = status;
            this.neighborhood = new List<PopulationItem>();
        }

        public PopulationItem()
        {
            
        }
    }


    public enum PopulationStatus
    {
        susceptible = 0,
        infected = 1,
        recovered = 2
    }
}