﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ResearchDataStructure
{
    class Program
    {
        private static bool simulationIsRunning = true;

        // Simulation constants
        static int N = 1000; // Population size
        static int Mp = 10; // Maximum number of partners per person
        static double Pf = 0.5; // Initial probability of forming a new partnership, gets mutated every tick
        private static double dPf = 1; // Rate at which Pf changes, Pf is multiplied by this value every tick
        static int totalPartnerships = 0; // A partnership is a relationship from A to B and B to A
        static int timesteps = 10; // Total number of timesteps

        // Create population
        public static List<PopulationItem> Population; // The population


        public static async Task Main()
        {
            
//            PerformanceTestRunner testRunner = new PerformanceTestRunner();
//            testRunner.GeneratePopulation();
//            testRunner.CreatePartnerships();
//            testRunner.AverageGlobalClustering(1000);





//            testRunner.GetNumberInfectedLINQ(10);
//            testRunner.CreatePartnerships();
//            testRunner.GetAverageNumberOfPartnerships();
//            testRunner.GetAverageNumberInfectedInNeighborhood();
//            Console.WriteLine("Creating population and partnerships");
            Console.WriteLine("Creating population and partnerships");


//            CreatePopulation();
//            CreatePartnerships();
            for (int i = 0; i < 10000; i++)
            {
                CreatePersistingPopulation();
            }

//            double avg = Population.Select(p => p.neighborhood.Count).Average();
//            double max = Population.Max(p => p.neighborhood.Count);
//            
//
//
////            CreatePopulation();
////            CreatePartnerships();
//            CreatePersistingPopulation();
//
////            double avg = Population.Select(p => p.neighborhood.Count).Average();
////            double max = Population.Max(p => p.neighborhood.Count);
////            
////
////            Console.WriteLine("Average number of partnerships:" + avg);
////            Console.WriteLine("Max number of partnerships: " + max);
//
//            //StartSimulation();
//
//            Console.WriteLine("Press E to exit the simulation, P to pause / resume");
//            while (simulationIsRunning)
//            {
//                ConsoleKey key = Console.ReadKey().Key;
//                if (key == ConsoleKey.E)
//                {
//                    Console.WriteLine("Exiting simulation");
//                    simulationIsRunning = false;
//                    Console.WriteLine("Simulation ended");
//
////                Task.();
//                }
//                else if (key == ConsoleKey.P)
//                {
//                    simulationIsRunning = false;
//                    Console.WriteLine("Simulation Paused");
//                    if (Console.ReadKey().Key == ConsoleKey.P)
//                    {
//                        Console.WriteLine("Simulation Resumed");
//                        simulationIsRunning = true;
//                        StartSimulation();
//                    }
//                }
//            }
        }

        private static void CreatePersistingPopulation()
        {
            Stopwatch sw = new Stopwatch();


            using (var db = new PopulationContext())
            {
                db.Database.EnsureCreated();
                Console.WriteLine("Resetting database");
                sw.Start();
//                db.PopulationItems.RemoveRange(db.PopulationItems);
                db.Database.ExecuteSqlRaw("TRUNCATE TABLE PopulationItems");
                sw.Stop();
                var rnd = new Random();

                Console.WriteLine($"Remove took {Math.Round(sw.Elapsed.TotalMilliseconds, 2)} milliseconds.");
                sw.Reset();
                sw.Start();
                for (int i = 0; i < N; i++)
                {
                    var p = new PopulationItem
                        {status = (PopulationStatus) Enum.GetValues(typeof(PopulationStatus)).GetValue(rnd.Next(0, 3))};
                    db.PopulationItems.Add(p);
                }

                db.SaveChanges();
                sw.Stop();
            using (var db2 = new LoggerContext())
            {
                Console.WriteLine(
                    $"Population created with {N} individuals. Took {Math.Round(sw.Elapsed.TotalMilliseconds, 2)} milliseconds.");

                db2.TestingLogItems.Add(new TestingLogItem(Math.Round(sw.Elapsed.TotalMilliseconds, 2),
                    N.ToString()));
                db2.SaveChanges();
            }
            }

        }

        public static async void StartSimulation()
        {
//            new System.Threading.Thread(async () =>
//            {
//                await Task.Run(() =>
//                {
//                    Task t = RunSimulation();
//                    t.RunSynchronously();
//                });
//            }).Start();
        }

        public static void CreatePopulation()
        {
            Population = new List<PopulationItem>(); // Create population list
            // Fill population list with new PopulationItems (people)
            for (int i = 0; i < N; i++)
                Population.Add(new PopulationItem(i, PopulationStatus.susceptible));

            Console.WriteLine($"Population created with {N} individuals.");
        }

        public static void CreatePartnerships()
        {
            int createdPartnerships = 0;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var rnd = new Random();

            for (int t = 0; t < timesteps; t++)
            {
                int currentCreatedPartnerships = 0;
                foreach (PopulationItem person in Population)
                {
                    // If the person has less than the maximum number of partners, continue
                    if (person.neighborhood.Count < Mp)
                    {
                        // Form a new partnership with probability Pf
                        if (rnd.Next(100) < (100 * Pf))
                        {
                            // New partnership gets created
                            currentCreatedPartnerships++;
                            PopulationItem partner = Population[rnd.Next(N)];

                            // Partnership is symmetrical
                            person.neighborhood.Add(partner);
                            partner.neighborhood.Add(person);
                        }
                    }
                }

                // Mutate Pf 
                Pf = Pf * dPf;

                createdPartnerships += currentCreatedPartnerships;
                totalPartnerships += createdPartnerships;

//                Console.WriteLine(
//                    $"Created {currentCreatedPartnerships} partnerships. Total partnerships currently created is {createdPartnerships}.");
            }

            sw.Stop();

            Console.WriteLine(
                $"Created {createdPartnerships} partnerships in {Math.Round(sw.Elapsed.TotalMilliseconds, 2)} milliseconds. Total amount of partnerships is {totalPartnerships}.");
        }


        public static Task RunSimulation()
        {
            int tick = 0;

            CreatePopulation();
            CreatePartnerships();


            Console.WriteLine();
            return new Task(async () =>
            {
                while (true)
                {
                    tick++;
//                    await Task.Delay(1000);
                    Console.Write("\rSimulation Tick {0}", tick);
                    if (!simulationIsRunning)
                    {
                        return;
                    }
                }
            });
        }
        

        public static IServiceProvider ConfigureServices(IServiceCollection serviceCollection)
        {
            //ILoggerFactory loggerFactory = new LoggerFactory()
            //  .AddConsole()
            //  .AddDebug();

            serviceCollection
                .AddLogging(opt =>
                {
                    opt.AddConsole();
                    opt.AddDebug();
                });

            /*... rest of config */

            var serviceProvider = serviceCollection.BuildServiceProvider();
            return serviceProvider;
        }
    }
}