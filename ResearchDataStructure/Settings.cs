namespace ResearchDataStructure
{
    public static class Settings
    {
        public static ConnectionType cType = ConnectionType.mysql;
    }

    public enum ConnectionType
    {
        mysql,
        sql
    }
}