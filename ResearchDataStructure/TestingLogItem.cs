using System;
using System.ComponentModel.DataAnnotations;

namespace ResearchDataStructure
{
    public class TestingLogItem
    {
        [Key] 
        public int LogItemId { get; set; }
        public double duration { get; set; }
        public string Values { get; set; }
        public long time { get; set; }
        public string ConnectionType { get; set; }

        public TestingLogItem(double duration, string values)
        {
            time = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            ConnectionType = Settings.cType.ToString();
            this.duration = duration;
            this.Values = values;
        }

        public TestingLogItem()
        {
            time =DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            ConnectionType = Settings.cType.ToString();
        }
    }
}